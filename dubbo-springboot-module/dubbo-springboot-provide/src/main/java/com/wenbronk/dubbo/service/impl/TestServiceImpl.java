package com.wenbronk.dubbo.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.wenbronk.dubbo.service.TestService;

@Service(version="1.0.0")
public class TestServiceImpl implements TestService {

	@Override
	public String test() {
		System.out.println("success");
		return "finally, i am coming";
	}
	
}
