package com.wenbronk.dubbo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.config.annotation.Reference;
import com.wenbronk.dubbo.service.TestService;

@RestController
public class TestController {

	@Reference(version="1.0.0")
	private TestService testService;
	
	@RequestMapping("/abc")
	public String test() {
		System.out.println("controller.sucess");
		testService.test();
		return "success";
	}
	
}
