package com.wenbronk.log.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

/**
 * Created by wenbronk on 2017/7/28.
 */
@Controller
class LogController {

    @GetMapping('/')
    def test() {
        "helloHtml"
    }

    @RequestMapping("/helloHtml")
    public String helloHtml(Map<String, Object> map) {
        map.put("hello", "from TemplateController.helloHtml");
        return "/helloHtml";
    }

    @RequestMapping('/connect')
    public String connect() {
        'test 2 connect sucess'
    }

}
