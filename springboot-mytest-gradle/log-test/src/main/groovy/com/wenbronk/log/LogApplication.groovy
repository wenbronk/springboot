package com.wenbronk.log

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * Created by wenbronk on 2017/7/28.
 */
@SpringBootApplication
class LogApplication {
    public static void main(String[] args) {
        SpringApplication.run(LogApplication.class, args)
    }
}
