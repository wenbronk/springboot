package com.wenbronk.es.test

import com.wenbronk.es.EsApplication
import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
/**
 * Created by wenbronk on 2017/9/11.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = [EsApplication.class])
class RESTClientTest {

    private RestClient restClient;

    @Test
    public void test1() {
        println restClient;
    }

    @Before
    public void before() {
        restClient = RestClient.builder(
                new HttpHost("192.168.192.129", 9200, "http")/*,new HttpHost("localhost", 9201, "http")*/).build();
    }

    @After
    public void after() {
        restClient.close();
    }

}
