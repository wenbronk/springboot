package com.wenbronk.security.entity
/**
 * Created by wenbronk on 2017/8/14.
 */
class SysRole {
    int id
    String name

    @Override
    public String toString() {
        return "SysRole{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
