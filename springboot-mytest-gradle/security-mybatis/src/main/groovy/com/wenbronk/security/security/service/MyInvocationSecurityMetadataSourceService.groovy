package com.wenbronk.security.security.service

import com.wenbronk.security.entity.SysPermission
import com.wenbronk.security.mapper.SysPermissionMapper
import org.springframework.security.access.ConfigAttribute
import org.springframework.security.access.SecurityConfig
import org.springframework.security.web.FilterInvocation
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.stereotype.Service

import javax.inject.Inject
import javax.servlet.http.HttpServletRequest

/**
 * Created by wenbronk on 2017/8/17.
 */
@Service
class MyInvocationSecurityMetadataSourceService implements FilterInvocationSecurityMetadataSource{

    @Inject
    SysPermissionMapper sysPermissionMapper

    /**
     * 此方法是为了判定用户请求的url 是否在权限表中，
     * 如果在权限表中，则返回给 decide 方法，用来判定用户是否有此权限。如果不在权限表中则放行。
     */
    @Override
    Collection<ConfigAttribute> getAttributes(Object o) throws IllegalArgumentException {
        Map<String, Collection<ConfigAttribute>> map = loadResourceDefine()
        HttpServletRequest request = ((FilterInvocation) o).getHttpRequest()
        map.each {entry ->
            AntPathRequestMatcher matcher = new AntPathRequestMatcher(entry.getKey())
            if (matcher.matches(request)) {
                // 匹配, 返回给decide方法
                return entry.getValue()
            }
        }
        return null
    }

    /**
     * 加载权限表中所有的权限
     */
    public Map<String, Collection<ConfigAttribute>> loadResourceDefine() {
        Map<String, Collection<ConfigAttribute>> map = new HashMap<>()
        List<SysPermission> permissions = sysPermissionMapper.findAll()
        permissions.each {permission ->
            List<ConfigAttribute> array = new ArrayList<>()
            // 此处只添加了用户的名字，其实还可以添加更多权限的信息，例如请求方法到ConfigAttribute的集合中去。
            // 此处添加的信息将会作为MyAccessDecisionManager类的decide的第三个参数。
            ConfigAttribute cfg = new SecurityConfig(permission.getName())
            array.add(cfg)
            // 用权限的url作为key, 权限的名称集合为value
            map.put(permission.getUrl(), array);
        }

        return null
    }

    @Override
    Collection<ConfigAttribute> getAllConfigAttributes() {
        return null
    }

    @Override
    boolean supports(Class<?> aClass) {
        return true
    }
}
