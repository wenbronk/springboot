package com.wenbronk.security.mapper

import com.wenbronk.security.entity.SysUser

/**
 * Created by wenbronk on 2017/8/14.
 */
interface SysUserMapper {
    SysUser findByUserName(String username)
}