package com.wenbronk.security.controller

import com.wenbronk.security.entity.Msg
import com.wenbronk.security.entity.SysUser
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Created by wenbronk on 2017/8/14.
 */
@Controller
class SecurityController {

    @RequestMapping(value = "/login")
    public Object login(@AuthenticationPrincipal SysUser loginUser, @RequestParam(name = "logout", required = false) String logout) {
        if (logout != null) {
            return null
        }
        if (loginUser != null) {
            return loginUser
        }
        return null
    }

    @RequestMapping("/")
    def index(Model model) {
        def msg = new Msg("测试标题", "测试内容", "额外信息, 只对管理员显示")
        model.addAttribute("msg", msg);
        "home"
    }

    @RequestMapping("/admin")
    @ResponseBody
    def hello() {
        'hello admin'
    }

}
