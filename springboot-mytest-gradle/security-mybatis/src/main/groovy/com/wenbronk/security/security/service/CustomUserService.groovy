package com.wenbronk.security.security.service

import com.wenbronk.security.entity.SysUser
import com.wenbronk.security.mapper.SysUserMapper
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

import javax.inject.Inject
/**
 * Created by wenbronk on 2017/8/15.
 */
@Service
class CustomUserService implements UserDetailsService {

    @Inject
    SysUserMapper sysUserMapper
//    @Inject
//    SysPermissionMapper sysPermissionMapper

    @Override
    UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        def sysUser = sysUserMapper.findByUserName(s) as SysUser
        assert sysUser != null

        List<GrantedAuthority> authorities = new ArrayList<>()
        sysUser.getRoles().each { role ->
            // 将用户的权限添加到 authrities中就可以了
            authorities.add(new SimpleGrantedAuthority(role.getName()))
        }
        sysUser.setAuthorities(authorities)
        return sysUser

    }
}
