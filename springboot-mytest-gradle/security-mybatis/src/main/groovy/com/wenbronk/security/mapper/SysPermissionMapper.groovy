package com.wenbronk.security.mapper

import com.wenbronk.security.entity.SysPermission

/**
 * Created by wenbronk on 2017/8/17.
 */
interface SysPermissionMapper {
    List<SysPermission> findAll()
    List<SysPermission> findByAdminUserId(int userId)
}
