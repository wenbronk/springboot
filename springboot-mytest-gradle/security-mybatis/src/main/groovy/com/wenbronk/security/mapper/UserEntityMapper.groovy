package com.wenbronk.security.mapper

import com.wenbronk.security.entity.UserEntity

/**
 * Created by wenbronk on 2017/8/14.
 */
interface UserEntityMapper {
    List<UserEntity> getAll();
    UserEntity getOne(Long id);
    void insert(UserEntity user);
    void update(UserEntity user);
    void delete(Long id);

}
