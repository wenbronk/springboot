package com.wenbronk.security.entity

/**
 * Created by wenbronk on 2017/8/17.
 */
class SysPermission {
    int id
    String name
    String desc
    String url
    int pid
}
