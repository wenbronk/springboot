package com.wenbronk.security.entity
/**
 * Created by wenbronk on 2017/8/14.
 */
class UserEntity {
    def id
    def userName
    def passWord
    def userSex
    def nickName

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", userName=" + userName +
                ", passWord=" + passWord +
                ", userSex=" + userSex +
                ", nickName=" + nickName +
                '}';
    }
}
