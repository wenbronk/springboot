package com.wenbronk.security.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

/**
 * Created by wenbronk on 2017/8/14.
 */
class SysUser implements UserDetails {
    int id
    String username
    @JsonIgnore
    String password
    String rawPass
    @JsonIgnore
    List<SysRole> roles
    List<? extends GrantedAuthority> authorities

    @Override
    @JsonIgnore
    Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities
    }

    @Override
    @JsonIgnore
    boolean isAccountNonExpired() {
        return true
    }

    @Override
    @JsonIgnore
    boolean isAccountNonLocked() {
        return true
    }

    @Override
    @JsonIgnore
    boolean isCredentialsNonExpired() {
        return true
    }

    @Override
    @JsonIgnore
    boolean isEnabled() {
        return true
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", rawPass='" + rawPass + '\'' +
                ", roles=" + roles +
                ", authorities=" + authorities +
                '}';
    }
}