package com.wenbronk.security.enums;

/**
 * Created by wenbronk on 2017/8/14.
 */
public enum UserSexEnum {
    MAN,
    WOMAN
}
