package com.wenbronk.security.controller

import com.wenbronk.security.entity.SysUser
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
/**
 * Created by wenbronk on 2017/8/17.
 */
@RestController
@RequestMapping("/users")
class HomeController {

    /**
     * 所有可访问
     */
    @RequestMapping(method = RequestMethod.GET)
    String getusers() {
        'getUsers'
    }

    @Secured(value = ['ROLE_ADMIN', 'ROLE_USER'])
    @RequestMapping(method = RequestMethod.POST)
    String save(@RequestBody SysUser user) {
        user.toString()
    }

    /**
     * 只有admin可访问
     */
    @Secured(value = ['ROLE_ADMIN'])
    @RequestMapping(method = RequestMethod.PUT)
    String update() {
        'updateUser'
    }

    @Secured(value = ['ROLE_ADMIN'])
    @RequestMapping(method = RequestMethod.DELETE)
    String delete() {
        'deleteUser'
    }

}
