package com.wenbronk.security.security.config

import org.springframework.security.access.AccessDecisionManager
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.access.ConfigAttribute
import org.springframework.security.authentication.InsufficientAuthenticationException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Service

/**
 * Created by wenbronk on 2017/8/17.
 */
@Service
class MyAccessDecisonManager implements AccessDecisionManager {

    /**
     * 判断是否拥有权限的决策方法
     * authentication 是CustomUserService中循环添加到 GrantedAuthority 对象中的权限信息集合.
     * object 包含客户端发起的请求的requset信息，可转换为 HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
     * configAttributes 为MyInvocationSecurityMetadataSource的getAttributes(Object object)这个方法返回的结果，
     */
    @Override
    void decide(Authentication authentication, Object o, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        if (null == configAttributes || configAttributes.size() <=0 )
            return
        configAttributes.each {configAttribute ->
            String needRole = configAttribute.getAttribute()
            authentication.getAuthorities().each {authority ->
                if (needRole.trim().equals(authority.getAuthority()))
                    return
            }
        }
        throw new java.nio.file.AccessDeniedException('no right')
    }

    @Override
    boolean supports(ConfigAttribute configAttribute) {
        return true
    }

    @Override
    boolean supports(Class<?> aClass) {
        return true
    }
}
