package com.wenbronk.security.security.interceptor

import com.wenbronk.security.security.config.MyAccessDecisonManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.SecurityMetadataSource
import org.springframework.security.access.intercept.AbstractSecurityInterceptor
import org.springframework.security.access.intercept.InterceptorStatusToken
import org.springframework.security.web.FilterInvocation
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource
import org.springframework.stereotype.Component

import javax.inject.Inject
import javax.servlet.*

/**
 * Created by wenbronk on 2017/8/17.
 */
@Component
class MyFilterSecurityInterceptor extends AbstractSecurityInterceptor implements Filter {

    @Inject
    FilterInvocationSecurityMetadataSource securityMetadataSource

    @Autowired
    public void setMyAccessDecisionManager(MyAccessDecisonManager myAccessDecisonManager) {
        super.setAccessDecisionManager(myAccessDecisonManager)
    }

    @Override
    void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * fi中有一个被拦截的url
     * 里面调用MyInvocationSecurityMetadataSource的getAttributes(Object object)这个方法获取fi对应的所有权限
     //再调用MyAccessDecisionManager的decide方法来校验用户的权限是否足够
     */
    @Override
    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        FilterInvocation fi = new FilterInvocation(request, response, chain)
        invoke(fi)
    }

    void invoke(FilterInvocation filterInvocation) {
        InterceptorStatusToken token = super.beforeInvocation(filterInvocation)
        try {
            filterInvocation.getChain().doFilter(filterInvocation.getRequest(), filterInvocation.getResponse())
        } finally {
            super.afterInvocation(token, null)
        }
    }

    @Override
    void destroy() {

    }

    @Override
    Class<?> getSecureObjectClass() {
        return FilterInvocation.class
    }

    @Override
    SecurityMetadataSource obtainSecurityMetadataSource() {
        return this.securityMetadataSource
    }
}
