package com.wenbronk.security.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
/**
 * Created by wenbronk on 2017/8/15.
 */
@Configuration
class WebMvcConfig extends WebMvcConfigurerAdapter{

    @Override
    void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login")
//        registry.addViewController("/home").setViewName("home")
    }
}
