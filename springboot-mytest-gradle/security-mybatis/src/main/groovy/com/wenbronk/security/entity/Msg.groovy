package com.wenbronk.security.entity

/**
 * Created by wenbronk on 2017/8/14.
 */
class Msg {
    String title
    String content
    String etraInfo

    Msg() {
    }

    Msg(String title, String content, String etraInfo) {
        this.title = title
        this.content = content
        this.etraInfo = etraInfo
    }
}
