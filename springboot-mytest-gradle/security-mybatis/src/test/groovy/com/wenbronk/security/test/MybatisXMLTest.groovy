package com.wenbronk.security.test

import com.wenbronk.security.mapper.SysPermissionMapper
import com.wenbronk.security.mapper.SysUserMapper
import com.wenbronk.security.mapper.UserEntityMapper
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

import javax.inject.Inject
/**
 * Created by wenbronk on 2017/8/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class MybatisXMLTest {

    @Inject
    UserEntityMapper userEntityMapper

    @Inject
    SysUserMapper sysUserMapper

    @Inject
    SysPermissionMapper sysPermissionMapper

    @Test
    void test3() {
        def perList = sysPermissionMapper.findAll()
        perList.each {println it}
    }

    @Test
    void test2() {
        def name = sysUserMapper.findByUserName('vini')
        println name
    }

    @Test
    void test1() {
        def one = userEntityMapper.getOne(29)
        println one
    }

}
