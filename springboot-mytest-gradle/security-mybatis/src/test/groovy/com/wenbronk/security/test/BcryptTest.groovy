package com.wenbronk.security.test

import org.junit.Test
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
/**
 * Created by wenbronk on 2017/8/17.
 */
class BcryptTest {

    @Test
    void test () {
        //进行加密
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        def pass = encoder.encode("admin")
        println pass
        println(pass.length())
    }
}
