package com.wenbronk.security.test

import com.wenbronk.security.entity.UserEntity
import com.wenbronk.security.enums.UserSexEnum
import com.wenbronk.security.mapper.UserEntityMapperAnno
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

import javax.inject.Inject
/**
 * Created by wenbronk on 2017/8/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class MybatisTest {

    @Inject
    UserEntityMapperAnno userMapper;

    @Test
    void insert() {
        userMapper.insert(new UserEntity(userName: 'vini', passWord: '123', userSex: UserSexEnum.WOMAN, nickName: 'H'))
        userMapper.insert(new UserEntity(userName: 'bronk', passWord: '123', userSex: UserSexEnum.MAN, nickName: 'H'))
    }

    @Test
    void query() {
        def find = userMapper.findAll()
        println find
    }

}


