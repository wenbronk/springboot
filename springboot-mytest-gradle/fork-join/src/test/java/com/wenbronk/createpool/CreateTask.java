package com.wenbronk.createpool;

import java.util.List;
import java.util.concurrent.RecursiveAction;

/**
 * Created by wenbronk on 2017/7/26.
 */
public class CreateTask extends RecursiveAction {

    private List<Product> products;
    private int first;
    private int last;
    private double increment;

    public CreateTask(List<Product> products, int first, int last, double increment) {
        this.products = products;
        this.first = first;
        this.last = last;
        this.increment = increment;
    }

    @Override
    protected void compute() {
        if (last - first < 10) {
            updatePrices();
        } else {
            int middle = (last + first) / 2;
            System.out.printf("task: pending tasks: %s \n", getQueuedTaskCount());

            CreateTask createTask = new CreateTask(products, first, middle + 1, increment);
            CreateTask createTask2 = new CreateTask(products, first, middle + 1, increment);
            invokeAll(createTask, createTask2);
        }
    }

    private void updatePrices() {
        for (int i = first; i < last; i++) {
            Product product = products.get(i);
            product.setPrice(product.getPrice() * (1 + increment));
        }
    }
}