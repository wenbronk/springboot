package com.wenbronk.createpool;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

/**
 * Created by wenbronk on 2017/7/26.
 */
public class CreateMain {
    public static void main(String[] args) {
        List<Product> generate = new ProductListGenerator().generate(10000);

        CreateTask createTask = new CreateTask(generate, 0, generate.size(), 2.20);
        ForkJoinPool forkJoinPool = new ForkJoinPool(20);
        forkJoinPool.execute(createTask);

        // 每5毫秒显示线程池变化信息
        do {
            System.out.printf("Main: threadCount: %d\n", forkJoinPool.getActiveThreadCount());
            System.out.printf("Main: getStealCount: %d\n", forkJoinPool.getStealCount());
            System.out.printf("Main: getParallelism: %d\n", forkJoinPool.getParallelism());

            try {
                TimeUnit.MICROSECONDS.sleep(50);
            }catch (Exception e) {
                e.printStackTrace();
            }

        }while ( !createTask.isDone());
        forkJoinPool.shutdown();;

        // 检查有没有出错的信息
        if (createTask.isCompletedNormally()) {
            System.out.println("Main: the process has completed normally !!! ");
        }else {
            System.out.println("this is an error in process");
        }

        // 将不是加个12的产品写入到控制台
        for (int i = 0; i < generate.size(); i++) {
            Product product = generate.get(i);
            if (product.getPrice() == 12) {
                System.out.printf("Product %s: %f \n", product.getName(), product.getPrice());
            }
        }
        System.out.println("end of the program!!!");
    }

}
