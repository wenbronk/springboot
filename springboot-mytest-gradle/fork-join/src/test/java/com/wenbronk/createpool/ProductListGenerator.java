package com.wenbronk.createpool;

import java.util.ArrayList;
import java.util.List;

/**
 * 产生随机产品的数列
 * Created by wenbronk on 2017/7/26.
 */
public class ProductListGenerator {

    public List<Product> generate (int size) {
        List<Product> ret = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            Product product = new Product();
            product.setName("Product " + i);
            product.setPrice(10d);
            ret.add(product);
        }
        return ret;
    }

}
