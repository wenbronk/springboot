package com.wenbronk.stream;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by wenbronk on 2017/8/11.
 */
public class StreamTest {



//    @Test
//    public void test13() {
//        Map<Boolean, List<Person>> children = Stream.generate(new PersonSupplier()).
//                limit(100).
//                collect(Collectors.partitioningBy(p -> p.getAge() < 18));
//        System.out.println("Children number: " + children.get(true).size());
//        System.out.println("Adult number: " + children.get(false).size());
//    }


    @Test
    public void test12() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("d:\\test.log"));
        int longest = br.lines().
                mapToInt(String::length).
                max().
                getAsInt();
        br.close();
        System.out.println(longest);
    }

    @Test
    public void test11() {
        List<String> list = Arrays.asList("2", "5", "2", "1", "8", "4", "3", "7", "9");
        List<String> list2 = list.stream().distinct().sorted((o1, o2) -> (Integer.parseInt(o2) - Integer.valueOf(o1))).collect(Collectors.toList());
        System.out.println(list2);
    }

    @Test
    public void test10() {
        // 字符串连接，concat = "ABCD"
        String concat = Stream.of("A", "B", "C", "D").reduce("", String::concat);
        // 求最小值，minValue = -3.0
        double minValue = Stream.of(-1.5, 1.0, -3.0, -2.0).reduce(Double.MAX_VALUE, Double::min);
        // 求和，sumValue = 10, 有起始值
        int sumValue = Stream.of(1, 2, 3, 4).reduce(0, Integer::sum);
        // 求和，sumValue = 10, 无起始值
        sumValue = Stream.of(1, 2, 3, 4).reduce(Integer::sum).get();
        // 过滤，字符串连接，concat = "ace"
        concat = Stream.of("a", "B", "c", "D", "e", "F").
                filter(x -> x.compareTo("Z") > 0).
                reduce("", String::concat);
    }

    @Test
    public void test9() {
        String str = "abc";
        // Java 8
        Optional.ofNullable(str).map(String::length).orElse(-1);
        // Pre-Java 8
        // return if (text != null) ? text.length() : -1;
    }

    @Test
    public void test8() {
        String str = "abc";
        // Java 8
        Optional.ofNullable(str).ifPresent(System.out::println);
        // Pre-Java 8
        if (str != null) {
            System.out.println(str);
        }
    }


    @Test
    public void test7() {
        List<Integer> nums = Arrays.asList(1, 1, 3, null, 2, null, 3, 4, 5, 8, 10);
        System.out.println(nums.stream().filter(num -> num != null).allMatch(num -> num < 8));
    }

    @Test
    public void test6() {
        List<Integer> nums = Arrays.asList(1, 1, 3, null, 2, null, 3, 4, 5, 8, 10);
        Integer count = nums.stream().filter(num -> num != null)
                .reduce((sum, num) -> sum + num).get();
        System.out.println(count);
    }

    @Test
    public void test5() {
        List<Integer> nums = Arrays.asList(1, 1, 3, null, 2, null, 3, 4, 5, 8, 10);
        List<Integer> collect = nums.stream().filter(num -> num != null)
                .collect(Collectors.toList());
        System.out.println(collect);
    }

    @Test
    public void test4() {
        List<Integer> nums = Arrays.asList(1, 1, 3, null, 2, null, 3, 4, 5, 8, 10);
        List<Integer> numsWithCollect = nums.stream().filter(num -> num != null)
                .collect(() -> new ArrayList<Integer>(),
                        (list, item) -> list.add(item),
                        (list1, list2) -> list1.addAll(list2));
        System.out.println(numsWithCollect);
    }

    @Test
    public void test3() {
        List<Integer> integers = Arrays.asList(1, 1, 3, null, 2, null, 3, 4, 5, 8, 10);
        System.out.println(integers.stream().filter(num -> num != null)
                .distinct()
                .mapToInt(num -> num * 10)
                .peek(System.out::println).skip(2).limit(4).sum());
    }

    @Test
    public void test2() {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6);
        Stream<Integer> stream = integers.stream();
    }

    @Test
    public void test() {
        // of
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 5);
        // generate, 无限长度, 懒加载, 类似工厂, 使用必须指定长度
        Stream<Double> generate = Stream.generate(Math::random);
        // iterator方法, 无限长度,
        Stream.iterate(1, item -> item + 1).limit(10).forEach(System.out::print);
    }

}
