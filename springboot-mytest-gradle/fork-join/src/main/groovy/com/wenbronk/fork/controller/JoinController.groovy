package com.wenbronk.fork.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by wenbronk on 2017/7/13.
 */
@RestController
class JoinController {

    @GetMapping("/")
    String test() {
        return 'connect success'
    }

}
