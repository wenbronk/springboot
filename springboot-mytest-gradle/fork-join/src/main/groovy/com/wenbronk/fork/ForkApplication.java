package com.wenbronk.fork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by wenbronk on 2017/7/13.
 */
@SpringBootApplication
public class ForkApplication {

    public static void main(String[] args) {
        SpringApplication.run(ForkApplication.class, args);
    }

}
