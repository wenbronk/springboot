package com.wenbronk.forkjoin.withresult;

import java.util.concurrent.RecursiveTask;

/**
 * 统计单词在一行出现的次数
 * Created by wenbronk on 2017/7/27.
 */
public class LineTask extends RecursiveTask<Integer> {
    private static final long seriaVersionUID = 1L;
    private String[] line;
    private int start, end;
    private String word;

    public LineTask(String[] line, int start, int end, String word) {
        this.line = line;
        this.start = start;
        this.end = end;
        this.word = word;
    }

    @Override
    protected Integer compute() {
        Integer result = null;
        if (end - start < 100) {
            result = count(line, start, end, word);
        }else {
            int mid = (start + end) / 2;
            LineTask task1 = new LineTask(line, start, mid, word);
            LineTask task2 = new LineTask(line, mid, end, word);
            invokeAll(task1, task2);
            try {
                result = groupResult(task1.get(), task2.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 合并2个数值的值, 返回结果
     * @return
     */
    private Integer groupResult(Integer num1, Integer num2) {
        return num1 + num2;
    }

    /**
     * 查找行中出现word的次数
     */
    private Integer count(String[] line, int start, int end, String word) {
        int counter = 0;
        for (int i = start; i < end; i++) {
            if (word.equals(line[i])) {
                counter ++;
            }
        }
//        try {
//            Thread.sleep(10);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return counter;
    }
}
