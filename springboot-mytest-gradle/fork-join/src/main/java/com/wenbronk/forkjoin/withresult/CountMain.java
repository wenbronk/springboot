package com.wenbronk.forkjoin.withresult;

import java.util.concurrent.ForkJoinPool;

/**
 * Created by wenbronk on 2017/7/27.
 */
public class CountMain {

    public static void main(String[] args) {
        Long start = System.currentTimeMillis();
        Document document = new Document();
        String[][] documents = document.generateDocument(100, 1000, "random");

        DocumentTask task = new DocumentTask(documents, 0, 100, "random");
        ForkJoinPool pool = new ForkJoinPool();
        pool.execute(task);

        do {
            System.out.printf("******************************************\n");
            System.out.printf("Main: Parallelism: %d\n",pool.getParallelism());
            System.out.printf("Main: Active Threads: %d\n",pool.getActiveThreadCount());
            System.out.printf("Main: Task Count: %d\n",pool.getQueuedTaskCount());
            System.out.printf("Main: Steal Count: %d\n",pool.getStealCount());
            System.out.printf("******************************************\n");
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        } while (!task.isDone());
        pool.shutdown();

        try {
            System.out.printf("Main: The word appears %d in the document ", task.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("耗时: " + (System.currentTimeMillis() - start));
    }
}
