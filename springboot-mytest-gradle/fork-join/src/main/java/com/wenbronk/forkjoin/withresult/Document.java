package com.wenbronk.forkjoin.withresult;

import java.util.Random;

/**
 * Created by wenbronk on 2017/7/26.
 */
public class Document {

    private String words[] = {"the", "hello", "goodbye", "pack", "java", "thread", "pool", "random", "class", "main"};

    public String[][] generateDocument(int numLines, int numWords, String word) {
        int counter = 0;
        String[][] document = new String[numLines][numWords];
        Random random = new Random();

        // 填充数组
        for (int i=0; i<numLines; i++){
            for (int j=0; j<numWords; j++) {
                int index=random.nextInt(words.length);
                document[i][j]=words[index];
                if (document[i][j].equals(word)){
                    counter++;
                }
            }
        }
        System.out.println(document.length + ": " + document[document.length - 1].length);

        System.out.println("DocumentMock: The word appears " + counter + " times in the document");
        return document;
    }

}
