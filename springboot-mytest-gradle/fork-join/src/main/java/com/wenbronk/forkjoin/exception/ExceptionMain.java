package com.wenbronk.forkjoin.exception;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

/**
 * Created by wenbronk on 2017/7/27.
 */
public class ExceptionMain {

    public static void main(String[] args) {
        int[] array = new int[100];

        Task task = new Task(array, 0, 100);

        ForkJoinPool forkJoinPool = new ForkJoinPool();
        forkJoinPool.execute(task);

        forkJoinPool.shutdown();

        try {
            // 使用awaitTermination()方法等待任务的结束。如果你想要等待任务的结束，无论它花多长时间结束，将值1和TimeUnit.DAYS作为参数传给这个方法。
            forkJoinPool.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 使用isCompletedAbnormally()方法，检查这个任务或它的子任务是否已经抛出异常
        if (task.isCompletedAbnormally()) {
            System.out.println("main::: an exception has occured \n");
            System.out.printf("main:::: %s \n", task.getException());
        }
        System.out.printf("main: result: %d", task.join());
    }

}
