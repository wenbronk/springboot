package com.wenbronk.forkjoin.exception;

import java.util.concurrent.RecursiveTask;
import java.util.concurrent.TimeUnit;

/**
 * forkjoin 中抛出异常的处理
 * Created by wenbronk on 2017/7/27.
 */
public class Task extends RecursiveTask<Integer> {

    private int array[];
    private int start, end;

    public Task(int[] array, int start, int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Integer compute() {
        System.out.printf("task: start from %d to %d \n", start, end);
        if (end - start < 10) {
            if ((3 > start) && (3 < end)) {
                System.out.println("paochu yichang " + start + " to " + end);
                throw new RuntimeException("task from " + start + " to " + end);
            }
            try {
                TimeUnit.SECONDS.sleep(1);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            int mid = (start + end) /2;
            Task task1 = new Task(array, start, mid);
            Task task2 = new Task(array, mid, end);
            invokeAll(task1, task2);
        }
        System.out.printf("task: end from %d to %d \n", start, end);
        return 0;
    }
}
