package com.wenbronk.forkjoin.async;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * Created by wenbronk on 2017/7/27.
 */
public class FolderProcessor extends RecursiveTask<List<String>> {
    private static final long serialVersionUID = 1l;

    private String path;
    private String extension;

    public FolderProcessor(String path, String extension) {
        this.path = path;
        this.extension = extension;
    }

    @Override
    protected List<String> compute() {
        // 保存存储在文件夹中的文件
        List<String> lists = new ArrayList<>();

        // 保存要处理的子任务
        List<FolderProcessor> tasks = new ArrayList<>();

        File file = new File(path);
        File[] content = file.listFiles();

        if (content != null) {
            for (int i = 0; i < content.length; i++) {
                if (content[i].isDirectory()) {
                    FolderProcessor task = new FolderProcessor(content[i].getAbsolutePath(), extension);
                    // 使用异步方法执行
                    task.fork();
                    tasks.add(task);
                }else {
                    if (checkFile(content[i].getName())) {
                        lists.add(content[i].getAbsolutePath());
                    }
                }
            }
            if (tasks.size() > 50) {
                System.out.printf("%s, %d tasks ran \n", file.getAbsolutePath(), tasks.size());
            }
            addResultFromTasks(lists, tasks);
            return lists;
        }

        return null;
    }

    private boolean checkFile(String name) {
        return name.endsWith(extension);
    }

    private void addResultFromTasks(List<String> lists, List<FolderProcessor> tasks) {
        for (FolderProcessor item : tasks) {
            lists.addAll(item.join());
        }
    }
}
