package com.wenbronk.forkjoin.withresult;

/**
 * 效率对比
 * Created by wenbronk on 2017/7/27.
 */
public class CompareClass {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        String[][] randoms = new Document().generateDocument(100, 1000, "random");

        int count = 0;

        for (String [] line : randoms) {
            for (String word : line) {
                if ("random".equals(word)) {
                    count ++;
                }
            }
        }
        System.out.println("this is " + count);
        System.out.println(System.currentTimeMillis() - start);
    }
}
