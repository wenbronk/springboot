package com.wenbronk.forkjoin.async;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

/**
 * Created by wenbronk on 2017/7/27.
 */
public class AsyncMain {

    public static void main(String[] args) {
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        FolderProcessor system = new FolderProcessor("c:\\windows", "log");
        FolderProcessor log = new FolderProcessor("c:\\Program Files", "log");
        FolderProcessor document = new FolderProcessor("c:\\Documents And Settings", "log");

        forkJoinPool.execute(system);
        forkJoinPool.execute(log);
        forkJoinPool.execute(document);

        do {
            System.out.printf("******************************************\n");
            System.out.printf("Main: Parallelism: %d\n",forkJoinPool.getParallelism());
            System.out.printf("Main: Active Threads: %d\n",forkJoinPool.getActiveThreadCount());
            System.out.printf("Main: Task Count: %d\n",forkJoinPool.getQueuedTaskCount());
            System.out.printf("Main: Steal Count: %d\n",forkJoinPool.getStealCount());
            System.out.printf("******************************************\n");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while((!system.isDone())||(!log.isDone())||(!document.isDone()));

        forkJoinPool.shutdown();

        List<String> results;
        results=system.join();
        System.out.printf("System: %d files found.\n",results.size());
        results=log.join();
        System.out.printf("Apps: %d files found.\n",results.size());
        results=document.join();
        System.out.printf("Documents: %d files found.\n",results.
                size());


    }

}
