package com.wenbronk.annotation.aspect;

import com.wenbronk.annotation.anno.MethodAction;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Created by wenbronk on 2017/8/16.
 */
@Aspect
@Component
public class MethodAspect {

    @Pointcut("@annotation(com.wenbronk.annotation.anno.MethodAction)")
    public void methodPoint() {}

    @After("methodPoint()")
    public void after(JoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        MethodAction annotation = method.getAnnotation(MethodAction.class);
        System.out.println("被拦截");
    }

}
