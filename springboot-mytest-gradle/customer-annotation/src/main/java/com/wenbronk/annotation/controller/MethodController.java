package com.wenbronk.annotation.controller;

import com.wenbronk.annotation.service.MethodService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

/**
 * Created by wenbronk on 2017/8/16.
 */
@RestController
public class MethodController {

    @Inject
    private MethodService methodService;

    @RequestMapping("/")
    public String test() {
        methodService.add();
        return "success";
    }

}
