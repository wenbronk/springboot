package com.wenbronk.annotation.anno;

import java.lang.annotation.*;

/**
 * Created by wenbronk on 2017/8/16.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MethodAction {
    String name();
}
