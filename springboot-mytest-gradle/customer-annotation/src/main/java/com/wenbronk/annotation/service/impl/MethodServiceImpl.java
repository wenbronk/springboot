package com.wenbronk.annotation.service.impl;

import com.wenbronk.annotation.anno.MethodAction;
import com.wenbronk.annotation.service.MethodService;
import org.springframework.stereotype.Service;

/**
 * Created by wenbronk on 2017/8/16.
 */
@Service
public class MethodServiceImpl implements MethodService {

    @Override
    @MethodAction(name = "方法拦截")
    public void add() {
        System.out.println("method inspect");
    }
}
