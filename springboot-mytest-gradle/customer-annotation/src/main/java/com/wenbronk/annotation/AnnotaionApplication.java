package com.wenbronk.annotation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by wenbronk on 2017/8/16.
 */
@SpringBootApplication
public class AnnotaionApplication {
    public static void main(String[] args) {
        SpringApplication.run(AnnotaionApplication.class);
    }
}
