package com.wenbronk.mvc.better.test;

import org.springframework.stereotype.Component;

/**
 * Created by wenbronk on 2017/5/16.
 */
@Component
public class DemoService {
    public String saySomething() {
        System.out.println("hello");
        return "hello";
    }
}
