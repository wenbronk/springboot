package com.wenbronk.mvc.base.controller;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by wenbronk on 2017/5/16.
 */
@Controller
public class HelloController {

    @RequestMapping("/index")
    public String hello() {
        return "index";
    }

    /**
     * produces 定制返回的数据类型和字符集
     * @param request
     * @return
     */
    @RequestMapping(value = {"/name", "/name2"}, produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String remove(HttpServletRequest request) {
        return "url" + request.getRequestURL() + "can access";
    }
}
