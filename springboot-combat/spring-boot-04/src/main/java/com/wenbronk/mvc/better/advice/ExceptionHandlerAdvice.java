package com.wenbronk.mvc.better.advice;

import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

/**
 * 添加@ControllerAdvice后, 可以将对于控制器的全局配置放在一个位置
 *     从而使@Controller 可使用   @ExceptionHandler处理全局异常
 *                              @InitBinder绑定前台请求参数到Model
 *                              @ModelAttribute 全局@RequestMapping 都能获取键值对
 *
 * Created by wenbronk on 2017/5/16.
 */
@ControllerAdvice
public class ExceptionHandlerAdvice {

    /**
     * 通过ExcpetionHandler 过滤拦截条件
     * @param exception
     * @param request
     * @return
     */
    @ExceptionHandler(value = {Exception.class})
    public ModelAndView exception(Exception exception, WebRequest request) {
        ModelAndView model = new ModelAndView("error");
        model.addObject("errorMessage", exception.getMessage());
        return model;
    }

    /**
     * @ModelAttribute 添加额外信息, 所有被@RequestMapping注解的方法都可获取此键值对
     * @param model
     */
    @ModelAttribute
    public void addAttribute(Model model) {
        model.addAttribute("msg", "额外信息");
    }

    /**
     * 通过InitBinder  注册定制WebDataBinder
     * @param webDataBinder
     */
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.setDisallowedFields("id");
    }

}
