package com.wenbronk.mvc.base.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * springmvc 配置
 * Created by wenbronk on 2017/5/16.
 */
@Configuration
@EnableWebMvc       // 开启默认配置, viewResolver或者messageConverter
@ComponentScan(basePackages = {"com.wenbronk.mvc.base"})
public class MyMvcConfig {

    /**
     * 设置配置jsp的viewResolver
     * @return
     */
    @Bean
    public InternalResourceViewResolver viewResoulver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/classes/views");
        viewResolver.setSuffix(".jsp");
        viewResolver.setViewClass(JstlView.class);
        return viewResolver;
    }
}
