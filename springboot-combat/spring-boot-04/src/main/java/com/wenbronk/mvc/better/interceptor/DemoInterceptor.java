package com.wenbronk.mvc.better.interceptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by wenbronk on 2017/5/16.
 */
public class DemoInterceptor extends HandlerInterceptorAdapter {

    /**
     * 拦截方法之前
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute("startTime", System.currentTimeMillis());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
        Long startTime = (Long) request.getAttribute("startTime");
        request.removeAttribute("startTime");

        System.out.println("请求处理的时间为: " + (System.currentTimeMillis() - startTime));
    }
}
