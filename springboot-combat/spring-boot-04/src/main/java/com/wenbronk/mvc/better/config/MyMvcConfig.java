package com.wenbronk.mvc.better.config;

import com.wenbronk.mvc.better.interceptor.DemoInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Created by wenbronk on 2017/5/16.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.wenbronk.mvc.better"})
public class MyMvcConfig extends WebMvcConfigurerAdapter {
    /**
     * 设置配置jsp的viewResolver
     * @return
     */
    @Bean
    public InternalResourceViewResolver viewResoulver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/classes/views");
        viewResolver.setSuffix(".jsp");
        viewResolver.setViewClass(JstlView.class);
        return viewResolver;
    }

    /**
     * 访问js资源
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
        // 对外暴露的路径
        registry.addResourceHandler("/js/**")
                // 文件放置目录
                .addResourceLocations("classpath:/js/");
    }

    /**
     * 添加自定义拦截器
     *
     * @param registry
     */
    @Bean
    public DemoInterceptor demoInterceptor() {
        return new DemoInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        registry.addInterceptor(demoInterceptor());
    }

    /**
     * 添加页面转向
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        super.addViewControllers(registry);
        
    }
}
