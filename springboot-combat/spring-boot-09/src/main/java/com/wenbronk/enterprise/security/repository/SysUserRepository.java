package com.wenbronk.enterprise.security.repository;

import com.wenbronk.enterprise.security.entity.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 数据访问层
 * Created by wenbronk on 2017/6/12.
 */
public interface SysUserRepository extends JpaRepository<SysUser, Long>{

    SysUser findByUsername(String username);

}
