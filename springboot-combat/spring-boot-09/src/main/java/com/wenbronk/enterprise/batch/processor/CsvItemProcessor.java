package com.wenbronk.enterprise.batch.processor;

import com.wenbronk.enterprise.batch.domain.Person;
import org.springframework.batch.item.validator.ValidatingItemProcessor;
import org.springframework.batch.item.validator.ValidationException;

/**
 * 数据校验及处理类, 继承校验处理类
 * Created by wenbronk on 2017/6/12.
 */
public class CsvItemProcessor extends ValidatingItemProcessor<Person> {

    /**
     * 处理从ItemReader读取到的数据, 返回数据给ItemWriter
     * @param item
     * @return
     * @throws ValidationException
     */
    @Override
    public Person process(Person item) throws ValidationException {
        super.process(item);            // 必须要, 调用自定义校验器

        if (item.getNation().equals("汉族")) {        // 数据简单处理
            item.setNation("01");
        }else {
            item.setNation("02");
        }
        return item;
    }
}
