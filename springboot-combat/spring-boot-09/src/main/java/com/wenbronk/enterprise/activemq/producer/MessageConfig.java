package com.wenbronk.enterprise.activemq.producer;

import com.wenbronk.enterprise.activemq.message.CreateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * 消息发送及目的地定义
 * 实现commandLineRunner, 使得程序启动后执行该类的run() 方法
 * Created by wenbronk on 2017/6/13.
 */
@Component
public class MessageConfig implements CommandLineRunner {

    @Autowired
    private JmsTemplate jmsTemplate;

    /**
     * 消息域为  my-destination
     * 发送者为 createMessage()
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        jmsTemplate.send("my-destination", new CreateMessage());
    }
}
