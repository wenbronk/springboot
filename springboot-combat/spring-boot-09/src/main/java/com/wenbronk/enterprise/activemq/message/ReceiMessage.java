package com.wenbronk.enterprise.activemq.message;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 消息接受者
 * Created by wenbronk on 2017/6/13.
 */
@Component
public class ReceiMessage {

    /**
     * 使用 @JmsListener 指定要监听的域, 有消息发送时就会发送到此域中
     * @param message
     */
    @JmsListener(destination = "my-destination")
    public void receiveMessage(String message) {
        System.out.println("接受到的消息是: " + message);
    }

}
