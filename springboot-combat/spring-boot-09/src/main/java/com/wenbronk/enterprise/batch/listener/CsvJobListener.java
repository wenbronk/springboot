package com.wenbronk.enterprise.batch.listener;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * job 监听
 * Created by wenbronk on 2017/6/13.
 */

public class CsvJobListener implements JobExecutionListener {

    private Long startTime;
    private Long endTime;

    @Override
    public void beforeJob(JobExecution jobExecution) {
        startTime = System.currentTimeMillis();
        System.out.println("任务处理开始: ");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        endTime = System.currentTimeMillis();
        System.out.println("任务处理结束");
        System.out.println("共耗时: " + (endTime - startTime));
    }
}
