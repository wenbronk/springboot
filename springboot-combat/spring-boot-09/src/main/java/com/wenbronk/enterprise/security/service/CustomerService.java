package com.wenbronk.enterprise.security.service;

import com.wenbronk.enterprise.security.entity.SysUser;
import com.wenbronk.enterprise.security.repository.SysUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * 自定义service, 实现UserDetailsService,
 * Created by wenbronk on 2017/6/12.
 */
public class CustomerService implements UserDetailsService {

    @Autowired
    private SysUserRepository sysUserRepository;

    /**
     * 重写, 获取用户
     * @param s
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        SysUser sysUser = sysUserRepository.findByUsername(s);
        if (sysUser == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        return sysUser;
    }
}
