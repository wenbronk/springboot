package com.wenbronk.enterprise.activemq.message;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 点对点和订阅模式的消息接受着
 * Created by wenbronk on 2017/6/13.
 */
@Component
public class MessageConsumer {

    @JmsListener(destination = "sample.topic")
    public void receiveQueue(String text) {
        System.out.println("Consumer2="+text);
    }

    @JmsListener(destination = "sample.topic")
    public void receiveTopic(String text) {
        System.out.println("Consumer3="+text);
    }

}
