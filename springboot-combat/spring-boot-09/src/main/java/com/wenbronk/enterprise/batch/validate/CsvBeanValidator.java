package com.wenbronk.enterprise.batch.validate;

import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.springframework.beans.factory.InitializingBean;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * 数据校验类
 * Created by wenbronk on 2017/6/12.
 */
public class CsvBeanValidator<T> implements Validator<T>, InitializingBean {
    private javax.validation.Validator validator;

    /**
     * 使用Validator的validate的方法去校验数据
     * @param t
     * @throws ValidationException
     */
    @Override
    public void validate(T t) throws ValidationException {
        Set<ConstraintViolation<T>> validate = validator.validate(t);
        if (validate.size() > 0) {
            StringBuilder message = new StringBuilder();
            for (ConstraintViolation ConstraintViolation : validate) {
                message.append(ConstraintViolation.getMessage() + "\n");
            }
            throw new ValidationException(message.toString());
        }

    }

    /**
     * 使用JSR-303的Validator校验数据, 进行Validator的初始化
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.usingContext().getValidator();
    }
}
