package com.wenbronk.enterprise.security.security;

import com.wenbronk.enterprise.security.service.CustomerService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * spring security 配置
 * 继承 WebSecurityConfigurerAdapter
 * Created by wenbronk on 2017/6/12.
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

    /**
     * 注册CustomUserService的bean
     * @return
     */
    @Bean
    public UserDetailsService customeUserService() {
        return new CustomerService();
    }

    /**
     * 添加自定义的user detail service 认证
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customeUserService());
    }

    /**
     * 请求权限
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated()           // 所有请求需要认证登陆
                .and()
                .formLogin()                            // 定制登陆操作
                    .loginPage("/login")
                    .failureUrl("/login?error")
                    .permitAll()                        // 登陆后可任意访问
                .and()
                .logout().permitAll();                  //登出后可任意访问
    }
}
