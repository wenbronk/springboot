package com.wenbronk.enterprise.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * batch 主要用来读取大量数据
 * Created by wenbronk on 2017/6/12.
 */
@SpringBootApplication
public class BatchApplicatin {
    public static void main(String[] args) {
        SpringApplication.run(BatchApplicatin.class, args);
    }
}
