package com.wenbronk.enterprise.rabbitmq.receive;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息接收者
 * Created by wenbronk on 2017/6/13.
 */
@Component
public class RabbitMQReceive {

    /**
     * 使用rabbitlistener接收消息
     * @param message
     */
    @RabbitListener(queues = "my-queue")
    public void receiveMessage(String message) {
        System.out.println("receive: " + message);
    }

}
