package com.wenbronk.enterprise.rabbitmq.send;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 消息发送者
 * 实现接口, 为了开机就自动发送
 * Created by wenbronk on 2017/6/13.
 */
@Component
public class RabbitMQSender implements CommandLineRunner{

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void run(String... args) throws Exception {
        rabbitTemplate.convertAndSend("my-queue", "来自rabiitmq的问候");
    }
}
