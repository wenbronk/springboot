drop table if exists PERSON;
create table PERSON
{
  id NUMBER not null primary key,
  name VARCHAR2(20),
  age INT ,
  nation VARCHAR2(20),
  address VARCHAR2(20)
}