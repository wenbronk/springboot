package com.wenbronk.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * jar包使用
 * @Springbootapplication 组合注解, 结合了
 *  @SpringBootConfiguration
    @EnableAutoConfiguration
    @ComponentScan

    通过exclude 可关闭特定的配置
 * Created by wenbronk on 2017/5/16.
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class App {

    public static void main(String[] args) {
//       SpringApplication.run(App.class, args);
        SpringApplication application = new SpringApplication(App.class);
        // 关闭banner, 版本高不管用了....
//        application.setShowBanner(false);
        application.run(args);
    }

}
