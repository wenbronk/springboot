package com.wenbronk.core.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 注入yml中的值
 * @ConfigurationProperties, 类型安装的注入方式, 将配置文件和一个bean相关联
 * 加上@property注解, 可以加载指定的yml文件或者properties文件, 也可通过此注解加载properties中的资源
 * Created by wenbronk on 2017/5/18.
 */
@Component
@ConfigurationProperties(prefix = "my.property")
// @PropertySource("classpath:server_url.yml")
public class YmlProperty {

    private String value;

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
