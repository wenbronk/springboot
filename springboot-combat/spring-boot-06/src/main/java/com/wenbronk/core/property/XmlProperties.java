package com.wenbronk.core.property;

import org.springframework.context.annotation.ImportResource;

/**
 * 加载xml配置, 比如xml配置的dubbo服务
 * 只加载既可以了
 * Created by wenbronk on 2017/5/18.
 */
@ImportResource({"classpath:value.xml", "classpath:what.xml"})
public class XmlProperties {
}
