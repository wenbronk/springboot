package com.wenbronk.core.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 自定义starter的配置类
 * Created by wenbronk on 2017/5/18.
 */
@ConfigurationProperties(prefix = "hello")
public class HelloProperties {

    public static final String MSG = "world";

    private String msg = MSG;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMSG() {
        return MSG;
    }
}
