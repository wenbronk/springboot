package com.wenbronk.core.autoconfig;

/**
 * 执行类
 * 根据这个类是否存在, 来创建这个类的Bean
 * Created by wenbronk on 2017/5/18.
 */
public class HelloService {

    private String msg;

    public String sayHello() {
        return "hello" + msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
