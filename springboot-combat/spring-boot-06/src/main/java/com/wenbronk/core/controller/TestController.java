package com.wenbronk.core.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wenbronk on 2017/5/18.
 */
@RestController
public class TestController {
    @RequestMapping("/test")
    public void test() {
        System.out.println("connect success");
    }
}
