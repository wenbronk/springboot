import com.wenbronk.core.App;
import com.wenbronk.core.property.YmlProperty;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by wenbronk on 2017/5/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class AppTest {

    @Autowired
    private YmlProperty ymlProperty;

    @Test
    public void contextLoads() {
        String value = ymlProperty.getValue();
        System.out.println(value);
    }

}
