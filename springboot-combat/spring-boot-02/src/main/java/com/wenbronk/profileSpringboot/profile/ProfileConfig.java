package com.wenbronk.profileSpringboot.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Created by root on 2017/5/13.
 */
@Component
public class ProfileConfig {

    @Bean(name = "myMessage")
    @Profile(value = "dev")
    public MyMessage devMsg() {
        return new MyMessage("dev");
    }
    @Bean(name = "myMessage")
    @Profile(value = "prod")
    public MyMessage prodMsg() {
        return new MyMessage("prod");
    }


}
