package com.wenbronk.profileSpringboot.profile;

/**
 * Created by root on 2017/5/13.
 */
public class MyMessage {
    private  String msg;

    public MyMessage(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {

        this.msg = msg;
    }
}
