package com.wenbronk.event;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by root on 2017/5/13.
 */
public class TestEvent {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(EventConfig.class);

        EventPublish publish = context.getBean(EventPublish.class);
        publish.publish("hello");
        context.close();
    }

}
