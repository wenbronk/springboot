package com.wenbronk.event;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by root on 2017/5/13.
 */
@SpringBootConfiguration
@ComponentScan(basePackages = {"com.wenbronk.event"})
public class EventConfig {

}
