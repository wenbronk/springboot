package com.wenbronk.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 事件发布类
 * Created by root on 2017/5/13.
 */
@Component
public class EventPublish  {

    /**
     * 注入publish用来发布事件
     */
    @Autowired
    private ApplicationContext applicationContext;

    public void publish(String msg) {
        applicationContext.publishEvent(new EventDemo(this, msg));
    }

}
