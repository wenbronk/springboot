package com.wenbronk.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 事件监听器
 * 泛型指定监听的类型
 * Created by root on 2017/5/13.
 */
@Component
public class EventListener implements ApplicationListener<EventDemo> {
    /**
     * 消息处理
     * @param eventDemo
     */
    @Override
    public void onApplicationEvent(EventDemo eventDemo) {
        String msa = eventDemo.getMsa();
        System.out.println(msa);
    }
}
