package com.wenbronk.event;

import org.springframework.context.ApplicationEvent;

/**
 * 自定义事件, 继承
 * Created by wenbronk on 2017/5/13.
 */
public class EventDemo extends ApplicationEvent {
    private static final long serialVersionUID = -739329440194879408L;

    private String msa;

    public EventDemo(Object source, String msa) {
        super(source);
        this.msa = msa;
    }

    public void setMsa(String msa) {
        this.msa = msa;
    }

    public String getMsa() {

        return msa;
    }

}
