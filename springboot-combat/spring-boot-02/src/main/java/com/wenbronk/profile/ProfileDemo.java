package com.wenbronk.profile;

import org.springframework.stereotype.Component;

/**
 * 测试profile
 * Created by wenbronk on 2017/5/13.
 */
@Component
public class ProfileDemo {

    private String content;

    public ProfileDemo(String content) {
        this.content = content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {

        return content;
    }
}
