package com.wenbronk.profile;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

/**
 * profiles的配置类
 * Created by wenbronk on 2017/5/13.
 */
@SpringBootConfiguration
public class ProfilesConfig {

    @Bean
    @Profile(value = "dev")
    public ProfileDemo devDemo() {
        return new ProfileDemo("dev");
    }

    @Bean
    @Profile(value = "prod")
    public ProfileDemo prodDemo() {
        return new ProfileDemo("prod");
    }

}
