package com.wenbronk.profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by wenbronk on 2017/5/13.
 */
public class TestProfile {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        // 设置环境, 可以通过注解  @ActiveProfiles("prod") 进行设置
        context.getEnvironment().setActiveProfiles("prod");
        context.register(ProfilesConfig.class);
        context.refresh();

        ProfileDemo demo = context.getBean(ProfileDemo.class);
        String content = demo.getContent();
        System.out.println(content);
        context.close();
    }

}
