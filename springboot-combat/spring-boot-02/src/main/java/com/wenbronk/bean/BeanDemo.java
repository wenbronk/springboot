package com.wenbronk.bean;

/**
 * init() destroy() bean形式的配置
 * Created by root on 2017/5/13.
 */
public class BeanDemo {
    public BeanDemo() {
        System.out.println("bean construct");
    }

    /**
     * init方法, 使用 @Bean配置
     */
    public void init() {
        System.out.println("bean init method");
    }
    /**
     * destroy, 在config中配置
     */
    public void destroy() {
        System.out.println("bean destroy method");
    }

}
