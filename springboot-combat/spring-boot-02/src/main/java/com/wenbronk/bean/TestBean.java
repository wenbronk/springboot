package com.wenbronk.bean;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 测试init和destroy方法
 * Created by root on 2017/5/13.
 */
public class TestBean {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(PrePostConfig.class);
        context.getBean(AnnotationDemo.class);
        context.getBean(BeanDemo.class);
        context.close();
    }


}
