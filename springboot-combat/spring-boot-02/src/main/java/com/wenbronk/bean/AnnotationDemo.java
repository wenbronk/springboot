package com.wenbronk.bean;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 使用注解形式的bean配置 init() 和destroy()
 * Created by root on 2017/5/13.
 */
@Component
public class AnnotationDemo {
    public AnnotationDemo() {
        System.out.println("annotation construct");
    }

    /**
     * 在构造函数之后执行
     */
    @PostConstruct
    public void init() {
        System.out.println(" annotation - init - method");
    }

    /**
     * 在对象销毁之前执行
     */
    @PreDestroy
    public void destroy() {
        System.out.println("annotation destroy method");
    }

}
