package com.wenbronk.bean;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by wenbronk on 2017/5/13.
 */
@SpringBootConfiguration
@ComponentScan(basePackages = {"com.wenbronk.bean"})
public class PrePostConfig {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public BeanDemo beanDemo() {
        return new BeanDemo();
    }

}
