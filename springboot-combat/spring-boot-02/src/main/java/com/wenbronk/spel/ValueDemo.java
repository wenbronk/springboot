package com.wenbronk.spel;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 注入类演示
 * Created by wenbronk on 2017/5/13.
 */
@Component
public class ValueDemo {

    @Value("其他类的树形")
    private String another;

    public String getAnother() {
        return another;
    }

    public void setAnother(String another) {
        this.another = another;
    }
}
