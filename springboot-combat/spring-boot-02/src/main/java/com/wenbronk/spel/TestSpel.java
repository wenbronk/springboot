package com.wenbronk.spel;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 *
 * Created by wenbronk on 2017/5/13.
 */
public class TestSpel {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpelDemo.class);
        SpelDemo demo = context.getBean(SpelDemo.class);
        try {
            demo.outputReources();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
