package com.wenbronk.spel;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;

import java.io.IOException;


/**
 * spel注入值的演示类
 * Created by wenbronk on 2017/5/13.
 */
@SpringBootConfiguration
@ComponentScan(basePackages = {"com.wenbronk.spel"})
@PropertySource("classpath:test.properties")        // 注入配置文件使用
public class SpelDemo {

    /**
     * string类型注入
     */
    @Value("string 类型注入")
    private String message;

    /**
     * 系统属性注入
     */
    @Value("#{systemProperties['os.name']}")
    private String osName;

    /**
     * 表达式值注入
     */
    @Value("#{ T(java.lang.Math).random() * 100 }")
    private double randomNumber;

    /**
     * 其他对象属性注入
     */
    @Value("#{valueDemo.another}")
    private String another;

    /**
     * 资源文件注入
     */
    @Value("classpath:test.txt")
    private Resource testFile;

    /**
     * 注入网址资源
     */
    @Value("https://www.baidu.com")
    private Resource testUrl;

    /**
     * 注入配置文件的值
     * 需要@PropertySource 和 PropertySourcePlaceholderConfigurer
     */
    @Value("${server.url}")
    private String serverUrl;
    @Autowired
    private Environment environment;

    public static PropertySourcesPlaceholderConfigurer getProperty() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    public void outputReources() throws IOException {
        System.out.println(message);
        System.out.println(osName);
        System.out.println(randomNumber);
        System.out.println(another);
        System.out.println(IOUtils.toString(testFile.getInputStream(), "utf-8"));
        System.out.println(IOUtils.toString(testUrl.getInputStream(), "utf-8"));
        System.out.println(serverUrl);

        // properties可以从Enviroment中获取
        System.out.println(environment.getProperty("server.version"));
    }

}
