package com.wenbronk.tomcat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 如果在app里配置tomcat, 需要声明为static
 * Created by wenbronk on 2017/5/18.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
         SpringApplication.run(Application.class, args);
    }

    /**
     * 需要声明为static
     */
//    public static class CustomerServletCOntainer implements EmbeddedServletContainerCustomizer {
//
//        @Override
//        public void customize(ConfigurableEmbeddedServletContainer container) {
//            container.setPort(8888);
//            container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/404.html"));
//            container.setSessionTimeout(10, TimeUnit.HOURS);
//        }
//    }

}
