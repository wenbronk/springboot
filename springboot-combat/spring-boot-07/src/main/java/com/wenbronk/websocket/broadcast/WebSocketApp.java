package com.wenbronk.websocket.broadcast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Created by wenbronk on 2017/5/19.
 */
@SpringBootApplication
public class WebSocketApp {

    public static void main(String[] args) {
        SpringApplication.run(WebSocketApp.class, args);
    }

}
