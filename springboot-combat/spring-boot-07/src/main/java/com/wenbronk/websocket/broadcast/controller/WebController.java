package com.wenbronk.websocket.broadcast.controller;

import com.wenbronk.websocket.broadcast.entity.MessageRequest;
import com.wenbronk.websocket.broadcast.entity.MessageResponse;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wenbronk on 2017/5/19.
 */
@RestController
public class WebController {
    @GetMapping("/test")
    public String test() {
        return "hello";
    }

    @MessageMapping("/welcome")     // 和requestMapping一样用
    @SendTo("/topic/getResponse")   // 向topic域中发送消息
    public MessageResponse say(MessageRequest message) {
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setResponseMsg(message.getRequestMsg());
        System.out.println("topic connect success");
        return messageResponse;
    }

}
