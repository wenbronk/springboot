package com.wenbronk.websocket.broadcast.entity;

/**
 *
 * 返回浏览器的消息
 * Created by wenbronk on 2017/5/19.
 */
public class MessageResponse {
    private String responseMsg;

    public MessageResponse() {
    }

    public MessageResponse(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResponseMsg() {

        return responseMsg;
    }
}
