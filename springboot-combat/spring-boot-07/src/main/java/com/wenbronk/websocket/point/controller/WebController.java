package com.wenbronk.websocket.point.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

/**
 * Created by wenbronk on 2017/5/22.
 */
@Controller
public class WebController {

    /**
     * 通过SpringTemplet 向前端发送消息
     */
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    /**
     * 点对点消息传输
     * @param principal
     * @param msg
     */
    @MessageMapping("/chat")
    public void handleChat(Principal principal, String msg) {
         if (principal.getName().equals("/vini")) {
             // 第一个是接收用户, 第二个是浏览器订阅地址, 第三个是消息
             messagingTemplate.convertAndSendToUser("wenbronk", "/queue/notifications", principal.getName() + "-send: " + msg);
         } else {
             messagingTemplate.convertAndSendToUser("vini", "/queue/notifications", principal.getName() + "-send: " + msg);
         }
    }


    @GetMapping("/hello")
    public String test() {
        return "hello";
    }

}
