package com.wenbronk.websocket.point.entity;

/**
 * Created by wenbronk on 2017/5/23.
 */
public class MessageEntity {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {

        return name;
    }
}
