package com.wenbronk.websocket.broadcast.entity;

/**
 * 消息接受类
 * Created by wenbronk on 2017/5/19.
 */
public class MessageRequest {

    private String requestMsg;

    public void setRequestMsg(String requestMsg) {
        this.requestMsg = requestMsg;
    }

    public String getRequestMsg() {
        return requestMsg;
    }


}
