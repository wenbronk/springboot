package com.wenbronk.websocket.broadcast.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

/**
 * 配置类
 * Created by wenbronk on 2017/5/19.
 */
@Configuration
@EnableWebSocketMessageBroker   // 开启使用 STOMP协议 传输基于代理的消息   此时浏览器支持 @MessageMapping, 就像@RequestMapping
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    /**
    // 注册一个STOMP 的endPoint, 并映射为指定的 URL
     * @param stompEndpointRegistry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
        // 注册一个STOMP的endPoint, 并指定使用的SockJs协议
        stompEndpointRegistry.addEndpoint("/endpointWisely").withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // 配置消息代理, 广播模式配置 /topic消息代理
        registry.enableSimpleBroker("/topic");
    }
}