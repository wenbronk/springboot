package com.wenbronk.websocket.point;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Created by wenbronk on 2017/5/19.
 */
@SpringBootApplication
public class WebSocketSecurityApp {

//    @Bean
//    public SimpMessagingTemplate simpMessagingTemplate() {
//        return  new SimpMessagingTemplate();
//    }

    public static void main(String[] args) {
        SpringApplication.run(WebSocketSecurityApp.class, args);
    }

}
