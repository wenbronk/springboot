package com.wenbronk.websocket.point.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * security 的简单配置
 * Created by wenbronk on 2017/5/22.
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/", "/login", "/hello").permitAll()         // 对 / 和 /login 不拦截
            .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login")                                // 设置登陆页面为 /login
                .defaultSuccessUrl("/chat")                                     // 登陆成功转向 /chat路径
                .permitAll().and().logout().permitAll();
    }

    /**
     * 在内存中设置2个用户, wenbronk 和 vini, 密码和用户名一致, 角色是USER
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("wenbronk").password("abc").roles("USER")
                .and()
                .withUser("vini").password("abc").roles("USER");
    }

    /**
     * 对resource下的静态资源不拦截
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/static/**");
    }
}
