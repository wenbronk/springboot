package com.wenbronk.health.check.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by wenbronk on 2017/6/14.
 */
@RestController
class Hello {

    @GetMapping("/hello")
    String hello() {
        "hello";
    }
}
