package com.wenbronk.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wenbronk on 2017/6/15.
 */
@RestController
public class SomeController {

    @Value("${my.message}")
    private String message;

    @RequestMapping(value = "/getSome")
    public String getSome() {
        return message;
    }

}
