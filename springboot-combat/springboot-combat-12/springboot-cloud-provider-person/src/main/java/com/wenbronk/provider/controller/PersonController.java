package com.wenbronk.provider.controller;

import com.wenbronk.provider.entity.PersonEntity;
import com.wenbronk.provider.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by wenbronk on 2017/6/15.
 */
@RestController
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public List<PersonEntity> savePerson(@RequestBody String personName) {
        PersonEntity personEntity = new PersonEntity(personName);
        personRepository.save(personEntity);

        List<PersonEntity> content = personRepository.findAll(new PageRequest(0, 10)).getContent();
        return content;
    }

}
