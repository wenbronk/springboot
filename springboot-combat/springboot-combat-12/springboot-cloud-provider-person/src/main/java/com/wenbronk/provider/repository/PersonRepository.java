package com.wenbronk.provider.repository;

import com.wenbronk.provider.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by wenbronk on 2017/6/15.
 */
public interface PersonRepository extends JpaRepository<PersonEntity, Long>{

    public PersonEntity save(PersonEntity personEntity);

    public PersonEntity findByName(String name);

}
