package com.wenbronk.consumer.ribbon;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * 使用ribbon调用 someService
 * Created by wenbronk on 2017/6/19.
 */
@Service
public class SomeHystrixService {

    /**
     * springboot中使用ribbon, 只需要注入restTemplate就可以了
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     *
     * @return
     */
    @HystrixCommand(fallbackMethod = "fallbackSome")
    public String getSome() {
        return restTemplate.getForObject("http://some/getsome", String.class);
    }

    public String fallbackSome() {
        return "some service 模块故障";
    }

}
