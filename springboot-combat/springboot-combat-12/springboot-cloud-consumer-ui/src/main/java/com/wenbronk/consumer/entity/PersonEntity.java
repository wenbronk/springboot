package com.wenbronk.consumer.entity;


/**
 * Created by wenbronk on 2017/6/15.
 */
public class PersonEntity {
    private Long id;
    private String name;
    private Integer age;
    private String adress;

    public PersonEntity() {
    }

    public PersonEntity(String name, Integer age, String address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.adress = adress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
