package com.wenbronk.consumer.controller;

import com.wenbronk.consumer.entity.PersonEntity;
import com.wenbronk.consumer.feign.PersonHystrixService;
import com.wenbronk.consumer.ribbon.SomeHystrixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 提供web支持
 * Created by wenbronk on 2017/6/19.
 */
@RestController
public class UIController {

    @Autowired
    private PersonHystrixService personHystrixService;

    @Autowired
    private SomeHystrixService someHystrixService;

    @GetMapping("/test1")
    public List<PersonEntity> hystrixTest(String name) {
        List<PersonEntity> save = personHystrixService.save(name);
        return save;
    }

    @GetMapping("/test2")
    public String ribbonTest() {
        String some = someHystrixService.getSome();
        return some;
    }

}
