package com.wenbronk.consumer.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.wenbronk.consumer.entity.PersonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wenbronk on 2017/6/15.
 */
@Service
public class PersonHystrixService {

    @Autowired
    private PersonFeignClient personFeignClient;

    @HystrixCommand(fallbackMethod = "fallbackSave")
    public List<PersonEntity> save(String name) {
        return personFeignClient.save(name);
    }

    public List<PersonEntity> fallbackSave(String name) {
        ArrayList<PersonEntity> list = new ArrayList<PersonEntity>();
        PersonEntity personEntity = new PersonEntity("Person service 故障", null, null);
        list.add(personEntity);
        return list;
    }

}
