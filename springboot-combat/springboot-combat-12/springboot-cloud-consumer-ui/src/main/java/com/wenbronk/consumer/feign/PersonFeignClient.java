package com.wenbronk.consumer.feign;

import com.wenbronk.consumer.entity.PersonEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 使用feign调用 person 服务
 * Created by wenbronk on 2017/6/15.
 */
@FeignClient(value = "person")
public interface  PersonFeignClient {

    /**
     * 注入person的rest服务
     * @param name
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<PersonEntity> save(@RequestBody String name);

}
