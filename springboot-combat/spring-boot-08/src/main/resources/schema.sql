drop table if exists person;
create table person (
	id int auto_increment,
	name varchar(40),
	age int(20),
	address varchar(40),
	primary key(id)
);