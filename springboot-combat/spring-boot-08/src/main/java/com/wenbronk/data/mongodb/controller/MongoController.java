package com.wenbronk.data.mongodb.controller;

import com.wenbronk.data.mongodb.entity.Person;
import com.wenbronk.data.mongodb.entity.PersonLocation;
import com.wenbronk.data.mongodb.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashSet;
import java.util.List;

/**
 * 使用jpa的方式操作mongodb
 * Created by wenbronk on 2017/6/12.
 */
@RestController
public class MongoController {

    @Autowired
    private PersonRepository personRepository;

    /**
     * 保存数据
     * @return
     */
    @RequestMapping("/save")
    public Person save() {
        Person vini = new Person("vini", 25, "bj");

        LinkedHashSet<PersonLocation> locatins = new LinkedHashSet<>();
        locatins.add(new PersonLocation("sd", "24"));
        locatins.add(new PersonLocation("bj", "2"));
        vini.setLocation(locatins);

        personRepository.save(vini);
        return vini;
    }

    /**
     * 根据名字查询
     * @param name
     * @return
     */
    @RequestMapping("/q1")
    public Person q1(String name) {
        return personRepository.findByName(name);
    }

    /**
     * 使用@query 查询
     * @param age
     * @return
     */
    @RequestMapping("/q2")
    public List<Person> q2(Integer age) {
        return personRepository.withQueryFindByAge(age);
    }
}
