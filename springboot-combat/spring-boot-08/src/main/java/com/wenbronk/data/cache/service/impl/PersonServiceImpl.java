package com.wenbronk.data.cache.service.impl;

import com.wenbronk.data.cache.entity.Person;
import com.wenbronk.data.cache.repository.PersonRepository;
import com.wenbronk.data.cache.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @Cacheable @CachePut @CacheEvit 都有key, value属性,
 *  value用于指定缓存名称,
 *      key指定存储的键, #开头, 如果不指定, 方法参数作为key
 * Created by wenbronk on 2017/6/8.
 */
@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository repository;

    /**
     * 存入缓存中, 于 @Cacheable保持一致
     *
     * 缓存名称是 people, 缓存的 key 是 id
     */
    @Override
    @CachePut(value = "people", key = "#person.id")
    public Person save(Person person) {
        Person p = repository.save(person);
        System.out.println("为id, key为: " + p.getId() + "数据做了缓存");
        return p;
    }

    /**
     * 删除缓存, 将一条或多条删除
     * @param id
     */
    @Override
    @CacheEvict(value = "people")
    public void remove(Long id) {
        System.out.println("删除了Id, key 为: " + id + "的缓存");
    }

    /**
     * Cacheable 方法执行前, 先去缓存中查找是否有数据, 没有则放入缓存
     */
    @Override
    @Cacheable(value = "people", key = "#person.id")
    public Person findOne(Person person) {
        Person one = repository.findOne(person.getId());
        System.out.println("id, key 为: " + one.getId() + "做了缓存");
        return one;
    }
}
