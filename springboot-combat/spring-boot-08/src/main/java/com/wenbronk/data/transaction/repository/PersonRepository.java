package com.wenbronk.data.transaction.repository;

import com.wenbronk.data.transaction.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by wenbronk on 2017/6/1.
 */

public interface PersonRepository extends JpaRepository<Person, Long>{
}
