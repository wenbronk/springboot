package com.wenbronk.data.jpa.contoller;

import com.wenbronk.data.jpa.entity.Person;
import com.wenbronk.data.jpa.repository.PersonRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 为了简化, 没有service
 * Created by wenbronk on 2017/5/23.
 */
@RestController
public class PersonController {

    @Resource
    private PersonRepository personRepository;

    /**
     * 保存, save支持批量保存
     * 删除: id删除, 批量删除
     *          void delete (ID id);    (T entity); (Iterable<? extends T> entities deleteAll()
     * @param name
     * @param address
     * @param age
     * @return
     */
    @GetMapping("/save")
    public Person save(Long id, String name, String address, Integer age) {
        Person save = personRepository.save(new Person(id, name, age, address));
        return save;
    }

    /**
     * 名称查询
     * @param address
     * @return
     */
    @RequestMapping("/findByAddress")
    public List<Person> findByAddress(String address) {
        List<Person> personList = personRepository.findByAddress(address);
        return personList;
    }

    /**
     * 自定义查询
     * @param name
     * @param address
     * @return
     */
    @RequestMapping("/findByNameAndAddress")
    public Person findByNameAndAddress(String name, String address) {
//        Person person = personRepository.findByNameAndAddress(name, address);
        Person person = personRepository.withNameAndAddressNamedQuery(name, address);
//        Person person = personRepository.withNameAndAddressQuery(name, address);
        return person;
    }

    /**
     * 排序
     * @return
     */
    @RequestMapping("/sort")
    public List<Person> sort() {
        List<Person> age = personRepository.findAll(new Sort(Sort.Direction.ASC, "age"));
        return age;
    }

    /**
     * 分页查询
     * @return
     */
    @RequestMapping("/page")
    public Page<Person> page() {
        Page<Person> page = personRepository.findAll(new PageRequest(1, 2));
        return page;
    }
}
