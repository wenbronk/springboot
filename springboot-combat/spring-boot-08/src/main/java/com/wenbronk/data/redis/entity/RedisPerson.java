package com.wenbronk.data.redis.entity;

import java.io.Serializable;

/**
 * Created by wenbronk on 2017/6/12.
 */
public class RedisPerson implements Serializable {
    private static final long serialVersionUID = -4458762601355914794L;

    private String id;
    private String name;
    private Integer age;

    public RedisPerson() {
    }

    public RedisPerson(String id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
