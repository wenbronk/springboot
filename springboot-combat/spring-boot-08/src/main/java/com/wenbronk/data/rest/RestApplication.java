package com.wenbronk.data.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by wenbronk on 2017/6/1.
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.data.rest"})
public class RestApplication {
    public static void main(String[] args) {
        SpringApplication.run(RestApplication.class, args);
    }
}
