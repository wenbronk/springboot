package com.wenbronk.data.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by wenbronk on 2017/6/1.
 */
@SpringBootApplication
//@EnableTransactionManagement  在springboot中, 不需要显示的开启
public class TransactionApplication {
    public static void main(String[] args) {
        SpringApplication.run(TransactionApplication.class, args);
    }
}
