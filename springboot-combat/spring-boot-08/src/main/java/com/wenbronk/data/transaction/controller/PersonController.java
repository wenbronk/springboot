package com.wenbronk.data.transaction.controller;

import com.wenbronk.data.transaction.entity.Person;
import com.wenbronk.data.transaction.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wenbronk on 2017/6/1.
 */
@RestController
public class PersonController {

    @Autowired
    private PersonService personService;

    @PostMapping("/rollback")
    public Person rollback(Person person) {
        return personService.savePersonWithRollBack(person);
    }

    @PostMapping("/noRollback")
    public Person noRollback(Person person) {
        return personService.savePersonWithoutRollBack(person);
    }

}
