package com.wenbronk.data.cache.controller;

import com.wenbronk.data.cache.entity.Person;
import com.wenbronk.data.cache.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 缓存请求类
 * Created by wenbronk on 2017/6/8.
 */
@RestController
public class CacheController {

    @Autowired
    private PersonService personService;

    /**
     * 存入缓存中
     * @param person
     * @return
     */
    @RequestMapping("/put")
    public Person put(Person person) {
        return personService.save(person);
    }

    /**
     * 获取cache
     * @param person
     * @return
     */
    @RequestMapping("/able")
    public Person cacheAble(Person person) {
        return personService.findOne(person);
    }

    /**
     * 删除一条或多条缓存
     * @param id
     * @return
     */
    @RequestMapping("/evit")
    public String evit(Long id) {
        personService.remove(id);
        return "ok";
    }

}
