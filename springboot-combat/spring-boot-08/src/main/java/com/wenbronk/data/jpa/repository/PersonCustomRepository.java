package com.wenbronk.data.jpa.repository;

import com.wenbronk.data.jpa.customer.repository.CustomerRepository;
import com.wenbronk.data.jpa.entity.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 使用自定义的repository
 * Created by wenbronk on 2017/6/1.
 */
public interface PersonCustomRepository extends CustomerRepository<Person,Long> {

    List<Person> findByAddress(String address);
    Person findByNameAndAddress(String name, String address);

    @Query("select p from Person p where p.name = :name and p.address = :address")
    Person withNameAndAddressQuery(@Param("name") String name, @Param("address") String address);

    Person withNameAndAddressNameQuery(String name, String address);

}
