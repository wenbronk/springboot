package com.wenbronk.data.mongodb.entity;

/**
 * 创建用户工作过的地点, location
 * Created by wenbronk on 2017/6/12.
 */
public class PersonLocation {

    private String place;
    private String year;

    public PersonLocation() {
    }

    public PersonLocation(String place, String year) {
        this.place = place;
        this.year = year;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
