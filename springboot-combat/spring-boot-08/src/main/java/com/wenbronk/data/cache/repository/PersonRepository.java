package com.wenbronk.data.cache.repository;

import com.wenbronk.data.cache.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by wenbronk on 2017/6/8.
 */
public interface PersonRepository extends JpaRepository<Person, Long> {
}
