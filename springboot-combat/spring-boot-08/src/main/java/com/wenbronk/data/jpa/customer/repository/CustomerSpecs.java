package com.wenbronk.data.jpa.customer.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.reflect.Field;
import java.util.ArrayList;

import static com.google.common.collect.Iterables.toArray;

/**
 * 自定义 Specification 的实现
 * 对象中的字符串默认like, 其他为equals, 没有则查询全部
 * Created by wenbronk on 2017/5/23.
 */
public class CustomerSpecs {

    public static <T> Specification<T> byAuto(final EntityManager entityManager, final T example) {
        // 获取类型
        final Class<T> type = (Class<T>) example.getClass();

        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                // 存储查询条件
                ArrayList<Predicate> predicates = new ArrayList<>();
                // 获取实体类的entitytype, 可以获取实体类的属性
                EntityType<T> entity = entityManager.getMetamodel().entity(type);

                for (Attribute<T, ?> attr: entity.getDeclaredAttributes()) {
                    // 获取实体类对象某一属性的值
                    Object attrValue = getValue(example, attr);
                    if (attrValue != null) {
                        // 属性为字符串时
                        if (attr.getJavaType() == String.class) {
                            if (!StringUtils.isEmpty(attrValue)) {
                                // 构建查询条件, 并添加列表中
                                predicates.add(criteriaBuilder.like(root.get(attribute(entity, attr.getName(), String.class)), patter((String) attrValue)));
                            }else {
                                predicates.add(criteriaBuilder.equal(root.get(attribute(entity, attr.getName(), attrValue.getClass())), attrValue));
                            }
                        }
                    }
                }
                // 将条件转成 predicate
                return predicates.isEmpty() ? criteriaBuilder.conjunction() : criteriaBuilder.and(toArray(predicates, Predicate.class));
            }

            /**
             * 通过反射获取实体类对象对应的属性值
             */
            private <T> Object getValue(T example, Attribute<T, ?> attr) {
                return ReflectionUtils.getField((Field)attr.getJavaMember(), example);
            }

            /**
             * 获得实体类的当前属性的SingularAttribute, SIngularAttribute包含的是实体类的某个单独属性
             */
            private <E, T> SingularAttribute<T, E> attribute(EntityType<T> entity, String fieldName, Class<E> fieldClass) {
                return entity.getDeclaredSingularAttribute(fieldName, fieldClass);
            }
        };
    }

    /**
     * 模糊查询
     * @param arg
     * @return
     */
    private static String patter(String arg) {
        return "%" + arg + "%";
    }

}
