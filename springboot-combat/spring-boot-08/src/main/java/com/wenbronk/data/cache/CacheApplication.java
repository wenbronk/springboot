package com.wenbronk.data.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * springboot的缓存支持
 * Created by wenbronk on 2017/6/8.
 */
@SpringBootApplication
@EnableCaching      // 开启缓存支持, 默认使用simpleCache(collection)
public class CacheApplication {
    public static void main(String[] args) {
        SpringApplication.run(CacheApplication.class, args);
    }
}
