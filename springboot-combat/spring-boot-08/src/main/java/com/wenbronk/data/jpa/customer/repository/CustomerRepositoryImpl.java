package com.wenbronk.data.jpa.customer.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * 继承 SimpleRepository 以使用其中的放法, 实现自定义接口方便调用
 * Created by wenbronk on 2017/5/24.
 */
public class CustomerRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements CustomerRepository<T, ID> {

    private final EntityManager entityManager;

    public CustomerRepositoryImpl(Class<T> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public Page<T> findByAuto(T example, Pageable pageable) {
//        return CustomerSpecs.find
        return findAll(CustomerSpecs.byAuto(entityManager, example), pageable);
    }
}
