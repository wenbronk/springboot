package com.wenbronk.data.cache.service;


import com.wenbronk.data.cache.entity.Person;

/**
 * Created by wenbronk on 2017/6/8.
 */
public interface PersonService {
    Person save(Person person);
    void remove(Long id);
    Person findOne(Person person);

}
