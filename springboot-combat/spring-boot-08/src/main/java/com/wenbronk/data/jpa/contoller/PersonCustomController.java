package com.wenbronk.data.jpa.contoller;

import com.wenbronk.data.jpa.entity.Person;
import com.wenbronk.data.jpa.repository.PersonCustomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 使用自定义的repository
 * Created by wenbronk on 2017/6/1.
 */
@RestController
public class PersonCustomController {

    @Autowired
    private PersonCustomRepository personCustomRepository;

    @GetMapping("/get")
    public Page<Person> auto(Person person) {
        Page<Person> byAuto = personCustomRepository.findByAuto(person, new PageRequest(0, 10));
        return byAuto;
    }

}
