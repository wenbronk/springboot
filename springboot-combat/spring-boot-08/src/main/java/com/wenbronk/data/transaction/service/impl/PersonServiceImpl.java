package com.wenbronk.data.transaction.service.impl;

import com.wenbronk.data.transaction.entity.Person;
import com.wenbronk.data.transaction.repository.PersonRepository;
import com.wenbronk.data.transaction.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wenbronk on 2017/6/1.
 */
@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public Person savePersonWithRollBack(Person person) {
        Person person1 = personRepository.save(person);
        if (person.getAge() instanceof String) {
            System.out.println("会回滚");
            throw new RuntimeException();
        }
        return person1;
    }

    @Override
    @Transactional(noRollbackFor = {Exception.class})
    public Person savePersonWithoutRollBack(Person person) {
        Person person1 = personRepository.save(person);
        if (person.getAge() instanceof String) {
            System.out.println("不会回滚");
            throw new RuntimeException();
        }
        return person1;
    }
}
