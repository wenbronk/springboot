package com.wenbronk.data.mongodb.repository;

import com.wenbronk.data.mongodb.entity.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * 可以使用类 jpa的方式 进行访问
 * 支持方法名欻性能
 * 支持@Query查询, 查询构造json
 * Created by wenbronk on 2017/6/12.
 */
public interface PersonRepository extends MongoRepository<Person, String> {

    /**
     * 使用方法名查询
     * @param name
     * @return
     */
    Person findByName(String name);

    /**
     * 使用 @Query 查询
     * @param age
     * @return
     */
    @Query("{'age': ?0}")
    List<Person> withQueryFindByAge(Integer age);

}
