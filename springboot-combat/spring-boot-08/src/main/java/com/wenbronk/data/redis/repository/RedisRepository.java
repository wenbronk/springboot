package com.wenbronk.data.redis.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * redis 的repository
 * Created by wenbronk on 2017/6/12.
 */
@Repository
public class RedisRepository {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    /**
     * 可以将StringRedisTemplate 注入到基于字符串的简单属性操作中
     */
    @Resource(name = "stringRedisTemplate")
    private ValueOperations<String, String> valueOperations;

    /**
     * 通过@Resrouce 将 redisTemplate 注入基于对象的简单属性操作中
     */
    @Resource(name = "redisTemplate")
    private ValueOperations<Object, Object> valOps;

    public void stringRedisSet() {
        valueOperations.set("xx", "yy");
    }

    public void redisSet() {
        valOps.set("vini", "123");
    }

    public String stringGet() {
        return valueOperations.get("xx");
    }

    public String get() {
        return (String) valOps.get("vini");
    }

}
