package com.wenbronk.data.mongodb.entity;


import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * Created by wenbronk on 2017/5/23.
 */
@Document
public class Person {

    @Id
    private long id;

    private String name;
    private Integer age;
    private String address;

    private Collection<PersonLocation> location = new LinkedHashSet<>();

    public Person() {
    }

    public Person(String name, Integer age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public Person(long id, String name, Integer age, String address, Collection<PersonLocation> location) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Collection<PersonLocation> getLocation() {
        return location;
    }

    public void setLocation(Collection<PersonLocation> location) {
        this.location = location;
    }
}
