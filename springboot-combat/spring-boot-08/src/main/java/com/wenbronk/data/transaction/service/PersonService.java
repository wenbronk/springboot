package com.wenbronk.data.transaction.service;

import com.wenbronk.data.transaction.entity.Person;

/**
 * Created by wenbronk on 2017/6/1.
 */
public interface PersonService {
    public Person savePersonWithRollBack(Person person);
    public Person savePersonWithoutRollBack(Person person);
}
