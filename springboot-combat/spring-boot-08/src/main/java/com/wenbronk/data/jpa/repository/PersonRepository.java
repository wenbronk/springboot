package com.wenbronk.data.jpa.repository;

import com.wenbronk.data.jpa.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 数据访问接口, jpa接口
 * Created by wenbronk on 2017/5/23.
 */
public interface PersonRepository extends JpaRepository<Person, Long> {

    List<Person> findByAddress(String name);

    Person findByNameAndAddress(String name, String address);

    @Query("select p from Person p where p.name = :name and p.address = :address")
    Person withNameAndAddressQuery(@Param("name") String name, @Param("address") String address);

    /**
     * 使用定义在实体类中的  nameQuery
     * @param name
     * @param address
     * @return
     */
    Person withNameAndAddressNamedQuery(String name, String address);

}
