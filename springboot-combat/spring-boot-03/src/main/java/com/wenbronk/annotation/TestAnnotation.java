package com.wenbronk.annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by wenbronk on 2017/5/15.
 */
public class TestAnnotation {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AnnotationConfig.class);
        DemoAnnotation bean = context.getBean(DemoAnnotation.class);
        bean.outputResult();
    }
}
