package com.wenbronk.annotation;

import org.springframework.stereotype.Component;

/**
 * Created by wenbronk on 2017/5/15.
 */
@Component
public class DemoAnnotation {
    public void outputResult() {
        System.out.println("从组合注解中获取bean");
    }
}
