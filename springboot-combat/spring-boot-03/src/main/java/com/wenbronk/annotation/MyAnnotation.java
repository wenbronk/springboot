package com.wenbronk.annotation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.lang.annotation.*;

/**
 * 自定义组合注解
 * 组合注解具备元注解的功能, 相当于extends ???
 * Created by wenbronk on 2017/5/15.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration
@ComponentScan
public @interface MyAnnotation {
    String[] Value() default  {};
}
