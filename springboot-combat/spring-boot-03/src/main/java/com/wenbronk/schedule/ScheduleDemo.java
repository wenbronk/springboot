package com.wenbronk.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * schedule 演示
 * Created by root on 2017/5/15.
 */
@Component
public class ScheduleDemo {
    private static final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    /**
     * @sheduled 声明为计划任务
     * fixedRate 定时执行
     */
    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        System.out.println("execut every five seconds " + dateFormat.format(new Date()));
    }

    @Scheduled(cron = "10 * * ? * *")
    public void fixTimeExecution() {
        System.out.println("在制定的时间执行: " + dateFormat.format(new Date()));
    }

}
