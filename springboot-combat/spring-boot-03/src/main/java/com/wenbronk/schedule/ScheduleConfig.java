package com.wenbronk.schedule;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

/**
 * Created by root on 2017/5/15.
 */
@Component
@ComponentScan(basePackages = {"com.wenbronk.schedule"})
@EnableScheduling       // 开启对计划任务的支持
public class ScheduleConfig {

}
