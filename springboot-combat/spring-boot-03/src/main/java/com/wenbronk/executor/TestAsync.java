package com.wenbronk.executor;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * test
 * Created by root on 2017/5/15.
 */
public class TestAsync {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AsyncConfig.class);
        AsyncTaskService bean = context.getBean(AsyncTaskService.class);

        for (int i = 0; i < 10; i++) {
            bean.executeAyncTask(i);
            bean.executeAsyncTaskPlus(i);
        }

        context.close();
    }
}
