package com.wenbronk.executor;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 任务执行类
 * Created by root on 2017/5/15.
 */
@Component
public class AsyncTaskService {

    /**
     * 使用async注解开启异步任务, 加载类上表示所有方法异步完成
     * @param integer
     */
    @Async
    public void executeAyncTask(Integer integer) {
        System.out.println("执行异步任务: " + integer);
    }

    @Async
    public void executeAsyncTaskPlus(Integer integer) {
        System.out.println("执行异步任务: " + integer + "====222");
    }

}
