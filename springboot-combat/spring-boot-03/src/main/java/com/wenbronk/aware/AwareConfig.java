package com.wenbronk.aware;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by root on 2017/5/14.
 */
@Configuration
@ComponentScan(value = {"com.wenbronk.aware"})
public class AwareConfig {

}
