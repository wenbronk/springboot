package com.wenbronk.aware;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * SpringAware 演示demo
 * BeanNameAware        获取容器中bean名称
 * BeanFactoryWare      获取当前bean factory
 * ApplicationContextAware 当前application context, 可以调用容器的服务
 * MessageSourceAware   获取message source, 可以获取文本信息
 * ApplicationEventPublisherAware 事件发布器
 * ResourceLoaderAware  获取资源加载, 获取外部资源文件
 *
 * Created by root on 2017/5/14.
 */
@Component
public class AwareService implements BeanNameAware, ResourceLoaderAware{

    private String beanName;
    private ResourceLoader resourceLoader;

    @Override
    public void setBeanName(String s) {
        this.beanName = s;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public void outputResult() throws IOException {
        System.out.println("bean's name is : " + beanName);
        Resource resource = resourceLoader.getResource("classpath:aware.txt");
        String str = IOUtils.toString(resource.getInputStream(), "utf-8");
        System.out.println("resource loader file is : " + str);
    }

}
