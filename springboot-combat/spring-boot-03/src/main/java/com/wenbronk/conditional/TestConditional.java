package com.wenbronk.conditional;

import com.wenbronk.conditional.service.OutputService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by root on 2017/5/15.
 */
public class TestConditional {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConditionalConfig.class);
        OutputService outputService = context.getBean(OutputService.class);

        String s = outputService.showCmd();
        System.out.println(s);

    }
}
