package com.wenbronk.conditional;

import com.wenbronk.conditional.os.LinuxConditional;
import com.wenbronk.conditional.os.WindowsConditional;
import com.wenbronk.conditional.service.LinuxOutput;
import com.wenbronk.conditional.service.OutputService;
import com.wenbronk.conditional.service.WindowsOutput;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * 通过 @conditional, 符合windows条件创建 windowsOutput, 否则创建linuxOutput
 * Created by root on 2017/5/15.
 */
@Configuration
public class ConditionalConfig {

    @Bean
    @Conditional(WindowsConditional.class)
    public OutputService windowsOutput() {
        return new WindowsOutput();
    }

    @Bean
    @Conditional(LinuxConditional.class)
    public OutputService LinuxOutput() {
        return new LinuxOutput();
    }
}
