package com.wenbronk.aop;

import org.springframework.stereotype.Component;

/**
 * Created by wenbronk on 2017/5/13.
 */
@Component
public class InterceptAnnotation {

    @AopAnnotation(value = "注解拦截")
    public void intercept() {
        System.out.println("annotation intercepting");
    }

}
