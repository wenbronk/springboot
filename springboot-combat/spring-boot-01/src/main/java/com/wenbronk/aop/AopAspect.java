package com.wenbronk.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 切面
 * Created by wenbronk on 2017/5/13.
 */
@Aspect
@Component
public class AopAspect {

    @Pointcut(value = "@annotation(com.wenbronk.aop.AopAnnotation)")   // 切点为注解
    public void pointCut() {};

    /**
     * 注解式拦截, 可在@After, @Before, @Aroud中直接引入 @pointCut
     * @param joinPoint
     */
    @After(value = "pointCut()")
    public void afterPointCut(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        AopAnnotation annotation = method.getAnnotation(AopAnnotation.class);
        System.out.println("注解式拦截: " + annotation.value());
    }


    /**
     * 方法式拦截
     * @param joinPoint
     */
    @Before(value = "execution(* com.wenbronk.aop.IntercepterMethod.*(..))")
    public void beforePointCut(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        System.out.println("方法式拦截: " + method.getName());
    }

}
