package com.wenbronk.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 测试类
 * Created by wenbronk on 2017/5/13.
 */
public class TestAop {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);

        // 注解式拦截
        InterceptAnnotation annotation = context.getBean(InterceptAnnotation.class);
        annotation.intercept();

        // 方法拦截
        IntercepterMethod method = context.getBean(IntercepterMethod.class);
        method.intercepter();

        context.close();

    }

}
