package com.wenbronk.aop;

import org.springframework.stereotype.Component;

/**
 * 方法拦截
 * Created by wenbronk on 2017/5/13.
 */
@Component
public class IntercepterMethod {
    public void intercepter() {
        System.out.println("method intercepter");
    }
}
