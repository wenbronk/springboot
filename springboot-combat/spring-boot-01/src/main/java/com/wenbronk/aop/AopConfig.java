package com.wenbronk.aop;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 配置类, 省却Application.java
 * Created by wenbronk on 2017/5/13.
 */
@SpringBootConfiguration
@ComponentScan(basePackages = {"com.wenbronk.aop"})
@EnableAspectJAutoProxy         // 开启对AspectJ的支持
public class AopConfig {


}
