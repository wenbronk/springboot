package com.wenbronk.config.annotation;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by wenbronk on 2017/5/12.
 */
@Service
public class TestController {

    @Resource
    private TestService testService;

    public String sayHello(String word) {
        return testService.sayHello(word);
    }

}
