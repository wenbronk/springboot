package com.wenbronk.config.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by root on 2017/5/12.
 */
public class TestJava {

    public static void main(String[] args ) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JavaConfig2.class);
        TestController bean = context.getBean(TestController.class);
        String hello = bean.sayHello("hello");
        System.out.println(hello);
    }


}


