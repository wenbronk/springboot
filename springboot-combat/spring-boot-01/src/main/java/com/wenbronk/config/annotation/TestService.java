package com.wenbronk.config.annotation;

import org.springframework.stereotype.Service;

/**
 * Created by wenbronk on 2017/5/12.
 */
@Service
public class TestService {

    public String sayHello(String hello) {
        return "Hello" + hello;
    }

}
