package com.wenbronk.config.java;

/**
 * Created by root on 2017/5/12.
 */
public class TestController {

    private TestService functionService;

    /**
     * 需要set方法
     * @param functionService
     */
    public void setFunctionService(TestService functionService) {
        this.functionService = functionService;
    }

    public String sayHello(String word) {
        return functionService.sayHello(word);
    }
}
