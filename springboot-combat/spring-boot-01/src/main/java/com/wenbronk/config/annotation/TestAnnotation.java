package com.wenbronk.config.annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by wenbronk on 2017/5/12.
 */

public class TestAnnotation {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AnnotationConfig.class);
        TestController user = context.getBean(TestController.class);
        String sayHello = user.sayHello("你好");
        System.out.println(sayHello);
    }
}
