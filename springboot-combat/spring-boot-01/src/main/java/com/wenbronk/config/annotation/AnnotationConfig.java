package com.wenbronk.config.annotation;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by wenbronk on 2017/5/12.
 */
@SpringBootConfiguration
@ComponentScan(value = {"com.wenbronk.config.annotation"})
public class AnnotationConfig {

}
