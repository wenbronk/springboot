package com.wenbronk.config.java;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * 使用javaConfig, 不使用注解
 * 配置类必须包含所有的bean
 * Created by root on 2017/5/12.
 */
@SpringBootConfiguration
public class JavaConfig {

    @Bean
    public TestService functionService() {
        return new TestService();
    }

    @Bean
    public TestController userFunctionService() {
        TestController testController = new TestController();
        testController.setFunctionService(functionService());
        return testController;
    }



}
