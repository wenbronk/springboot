package com.wenbronk.config.java;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * 除了使用上面那种方法, 还可以直接new
 * Created by wenbronk on 2017/5/12.
 */
@SpringBootConfiguration
public class JavaConfig2 {

    @Bean
    public TestService functionService() {
        return new TestService();
    }

    /**
     * spring容器中有的bean, 可以直接作为参数传入
     * @param functionService
     * @return
     */
    @Bean
    public TestController userFunctionService(TestService functionService) {
        TestController testController = new TestController();
        testController.setFunctionService(functionService);
        return testController;
    }

}
