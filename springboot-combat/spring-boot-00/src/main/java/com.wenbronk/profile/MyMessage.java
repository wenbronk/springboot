package com.wenbronk.profile;

/**
 * Created by wenbronk on 2017/5/13.
 */
public class MyMessage {
    private  String msg;

    public MyMessage(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {

        this.msg = msg;
    }
}
