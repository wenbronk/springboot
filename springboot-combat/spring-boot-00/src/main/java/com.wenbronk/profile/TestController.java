package com.wenbronk.profile;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wenbronk on 2017/5/13.
 */
@RestController
public class TestController {

    @Autowired
    private MyMessage myMessage;

    @RequestMapping("/")
    public void test() {
        System.out.println("success");
        System.out.println(myMessage.getMsg());
    }

}
