package com.wenbronk.annotation.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wenbronk.annotation.springboot.annotation.RequestLimit;

/**
 * 访问controller层
 * 
 * @author wenbronk
 * @time 2017年5月12日
 */
@RestController
public class URLController {
	
	@RequestMapping(value="/test")
	@RequestLimit(count=2, time=600000)
	public String test() {
		System.out.println("i am here");
		return "success";
	}
	
}
