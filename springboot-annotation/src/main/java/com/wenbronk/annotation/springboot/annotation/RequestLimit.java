package com.wenbronk.annotation.springboot.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * 自定义注解, 实现访问控制
 * 
 * @author wenbronk
 * @time 2017年5月12日
 */

@Retention(RetentionPolicy.RUNTIME)  
@Target(ElementType.METHOD)  
@Documented  
//最高优先级  
@Order(Ordered.HIGHEST_PRECEDENCE)
public @interface RequestLimit {

	/**
	 * 允许访问的次数
	 * @return
	 * @time 2017年5月12日
	 */
	int count() default Integer.MAX_VALUE;
	
	/**
	 * 默认时间
	 * @return
	 * @time 2017年5月12日
	 */
	long time() default 60000;
	
}
