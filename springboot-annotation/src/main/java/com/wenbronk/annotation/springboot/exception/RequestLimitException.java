package com.wenbronk.annotation.springboot.exception;

/**
 * 自定义异常类, 超限抛出此异常
 * 
 * @author wenbronk
 * @time 2017年5月12日
 */
public class RequestLimitException extends Exception {
	private static final long serialVersionUID = -3372256433309280367L;
	
	public RequestLimitException(String message) {  
		super(message);  
	}
	
	public RequestLimitException() {  
        super("HTTP请求超出设定的限制");  
    }  
	
}
