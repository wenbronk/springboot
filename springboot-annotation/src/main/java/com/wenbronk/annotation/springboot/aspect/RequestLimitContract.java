package com.wenbronk.annotation.springboot.aspect;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.wenbronk.annotation.springboot.annotation.RequestLimit;
import com.wenbronk.annotation.springboot.exception.RequestLimitException;

/**
 * 注解的执行类
 * @author wenbronk
 * @time 2017年5月12日
 */
@Aspect
@Component
public class RequestLimitContract {
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestLimitContract.class);
	
	/**模拟redis*/
	private ConcurrentMap<String, Integer> redisTemplate=new ConcurrentHashMap<String,Integer>();
	
	
//	 @Pointcut("within(@org.springframework.web.bind.annotation.RestController *) && @annotation(requestLimit)")
	 public void aopDemo() {
		 System.out.println("拦截生效");
	 }
	
	@Before("within(@org.springframework.web.bind.annotation.RestController *) && @annotation(requestLimit)")
	public void requestLimit(JoinPoint joinPoint, RequestLimit requestLimit) throws RequestLimitException {
		try {
//			Object[] args = joinPoint.getArgs();
//			HttpServletRequest request = null;
//			for(int i = 0; i < args.length; i++) {
//				if (args[i] instanceof HttpServletRequest) {
//					request = (HttpServletRequest) args[i];
//					break;
//				}
//			}
//			if (request == null) {
//				throw new RequestLimitException("方法中缺失HttpServletRequest参数");
//			}
			// 接收到请求，记录请求内容
	        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
	        HttpServletRequest request = attributes.getRequest();
			
			String ip = request.getLocalAddr();  
            String url = request.getRequestURL().toString();  
            String key = "req_limit_".concat(url).concat(ip);  
            if(redisTemplate.get(key)==null || redisTemplate.get(key)==0){  
                redisTemplate.put(key,1);  
            }else{  
                redisTemplate.put(key,redisTemplate.get(key)+1);  
            }  
            int count = redisTemplate.get(key);
            if (count > 0) {  
                Timer timer= new Timer();  
                TimerTask task  = new TimerTask(){    //创建一个新的计时器任务.
                    @Override  
                    public void run() {  
                        redisTemplate.remove(key);  
                    }  
                };  
                timer.schedule(task, requestLimit.time());  
                //安排在指定延迟后执行指定的任务。task : 所要安排的任务。10000 : 执行任务前的延迟时间，单位是毫秒。  
            }  
            if (count > requestLimit.count()) {  
                LOGGER.info("用户IP[" + ip + "]访问地址[" + url + "]超过了限定的次数[" + requestLimit.count() + "]");  
                throw new RequestLimitException();  
            }  
		}catch (RequestLimitException e) {
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("发生内部错误");
		}
	}
	
}
