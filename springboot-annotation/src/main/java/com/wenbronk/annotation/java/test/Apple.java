package com.wenbronk.annotation.java.test;

import com.wenbronk.annotation.java.FruitColor;
import com.wenbronk.annotation.java.FruitName;
import com.wenbronk.annotation.java.FruitProvider;
import com.wenbronk.annotation.java.FruitColor.Color;

public class Apple {

    @FruitName(value="FuShi Apple")
    private String fruitName;
    
    @FruitColor(fruitColor=Color.RED)
    private String fruitColor;
    
    @FruitProvider(id=1,user="Tom",address="China")
    private FruitProvider provider;
}
