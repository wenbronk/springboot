package com.wenbronk.annotation.method.service;

import org.springframework.stereotype.Service;

import com.wenbronk.annotation.method.Action;

/**
 * 使用注解的被拦截类
 * 
 * @author wenbronk
 * @time 2017年5月12日
 */
@Service
public class DemoAnnotationService {
	
	@Action(name="注解式拦截的add操作")
	public void add() {}
	
}
