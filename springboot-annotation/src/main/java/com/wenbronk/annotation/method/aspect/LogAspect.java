package com.wenbronk.annotation.method.aspect;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.wenbronk.annotation.method.Action;

/**
 * 切面
 * 
 * @author wenbronk
 * @time 2017年5月12日
 */
@Aspect
@Component
public class LogAspect {

	/**切点为注解*/
	@Pointcut(value="@annotation(com.wenbronk.annotation.method.Action)")
	public void annotationPointCut() {};
	
	/**
	 * 使用注解拦击
	 * @param joinPoint
	 * @time 2017年5月12日
	 */
	@After("annotationPointCut()")
	public void after(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		
		Action annotation = method.getAnnotation(Action.class);
		System.out.println("注解式拦截的方法为: " + annotation.name());
	}
	
	/**
	 * 使用方法拦截
	 * @param joinPoint
	 * @time 2017年5月12日
	 */
	@Before("execution (* com.wenbronk.annotation.method.service.DemoMethodService.add(..))")
	public void before(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		
		Action annotation = method.getAnnotation(Action.class);
		System.out.println("方法式拦截的方法为: " + annotation.name());
	}
	
}
