package com.wenbronk.annotation.method;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.wenbronk.annotation.method.config.AopConfig;
import com.wenbronk.annotation.method.service.DemoAnnotationService;
import com.wenbronk.annotation.method.service.DemoMethodService;

/**
 * 测试类
 * 
 * @author wenbronk
 * @time 2017年5月12日
 */
public class MehtodAopTest {

	@Test
	public void test() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);
		DemoAnnotationService bean = context.getBean(DemoAnnotationService.class);
		bean.add();
		
		DemoMethodService methodService = context.getBean(DemoMethodService.class);
		methodService.add();
		
		context.close();
	}
	
}
