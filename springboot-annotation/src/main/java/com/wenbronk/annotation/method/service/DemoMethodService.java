package com.wenbronk.annotation.method.service;

import org.springframework.stereotype.Service;

/**
 * 方法规则被拦截类
 * 
 * @author wenbronk
 * @time 2017年5月12日
 */
@Service
public class DemoMethodService {

	public void add() {}
	
}
