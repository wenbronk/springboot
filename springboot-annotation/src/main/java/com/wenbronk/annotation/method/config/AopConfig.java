package com.wenbronk.annotation.method.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 配置类
 * 
 * @author wenbronk
 * @time 2017年5月12日
 */
@Configuration
@ComponentScan("com.wenbronk.annotation.method")
@EnableAspectJAutoProxy
public class AopConfig {

}
