package com.wenbronk.dubbo.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wenbronk.dubbo.test.service.TestService;

@RestController
public class TestController {

	@Autowired
	private TestService testService;
	
	@RequestMapping("/")
	public void test() {
		System.out.println("connect");
		testService.test();
	}
	
}
