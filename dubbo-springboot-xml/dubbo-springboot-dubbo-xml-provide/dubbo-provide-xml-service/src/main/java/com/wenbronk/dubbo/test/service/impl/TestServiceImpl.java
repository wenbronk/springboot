package com.wenbronk.dubbo.test.service.impl;

import org.springframework.stereotype.Service;

import com.wenbronk.dubbo.test.service.TestService;

@Service
public class TestServiceImpl implements TestService {

	@Override
	public String test() {
		System.out.println("success");
		return "success";
	}

}
