package com.holy.dm

import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.ServletComponentScan

/**
 * @SpringBootApplication: 等价于以默认属性使用 @Configuration ， @EnableAutoConfiguration 和 @ComponentScan
 */
@SpringBootApplication
@MapperScan("com.holy.dm.mapper")
class DruidMybatisApplication {

	static void main(String[] args) {
		SpringApplication.run DruidMybatisApplication, args
	}
}
