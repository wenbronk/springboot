package com.holy.dm.controller

import com.holy.dm.domain.User
import com.holy.dm.service.UserService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by fengyoutian on 2017/4/16.
 */

@RestController // 给全局的方法加上@ResponseBody，使用@Contoller时需自行在方法前加上@ResponseBody
class UserController {

    Logger logger = LoggerFactory.getLogger UserController.class

    @Autowired
    UserService userService

    @RequestMapping('/')
    hello() {
        'Hello World!'
    }

    @RequestMapping('/getAll')
    List<User> getAll() {
        logger.info 'User getAll'
        userService.getAll()
    }

    @RequestMapping('/getOne/{id}')
    User getById(@PathVariable(name = 'id') Integer id) {
        logger.info 'User getById:{}', id
        userService.getById id
    }
}
