package com.holy.dm.service

import com.holy.dm.domain.User
import com.holy.dm.mapper.UserMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Created by fengyoutian on 2017/4/16.
 */
@Service
class UserService {

    @Autowired
    UserMapper userMapper

    List<User> getAll() {
        userMapper.selectAll()
    }

    User getById(Integer id) {
        userMapper.selectById id
    }
}
