package com.holy.dm.mapper

import com.holy.dm.domain.User
import org.springframework.stereotype.Repository

/**
 * Created by fengyoutian on 2017/4/16.
 */

@Repository
interface UserMapper {
    List<User> selectAll()

    User selectById(Integer id)
}
