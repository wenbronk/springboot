package com.holy.dm.domain
/**
 * Created by fengyoutian on 2017/4/14.
 */
class User {

    /** 主键 */
    def id

    /** 用户名 */
    def userName

    /** 密码 */
    def password

    /** 手机号 */
    def phone

    /** 邮箱 */
    def email

    /** 创建时间 */
    def createTime


    @Override
    String toString() {
        'User{' +
            'id=' + id +
            ', userName=' + userName +
            ', password=' + password +
            ', phone=' + phone +
            ', email=' + email +
            ', createTime=' + createTime +
            '}'
    }
}
