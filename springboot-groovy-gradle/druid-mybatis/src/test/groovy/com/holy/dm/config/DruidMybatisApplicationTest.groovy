package com.holy.dm.config

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

/**
 * Created by fengyoutian on 2017/4/17.
 */

@RunWith(SpringRunner)
@SpringBootTest
class DruidMybatisApplicationTest {

	@Test
	void contextLoads() {
	}

}