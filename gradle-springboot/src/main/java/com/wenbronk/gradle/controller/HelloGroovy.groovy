package com.wenbronk.gradle.controller

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by wenbronk on 2017/6/27.
 */
@RestController
class HelloGroovy {

    @RequestMapping("/")
    String home() {
        return "Hello groovy!"
    }

}
