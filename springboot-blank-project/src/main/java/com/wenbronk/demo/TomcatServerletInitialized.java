package com.wenbronk.demo;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author wenbronk
 * @time 2017年7月19日 下午1:48:19
 */
public class TomcatServerletInitialized extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootBlankProjectApplication.class);
	}

}
