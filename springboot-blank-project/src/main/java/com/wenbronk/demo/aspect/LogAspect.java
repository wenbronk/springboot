package com.wenbronk.demo.aspect;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.wenbronk.demo.tools.DateUtils;

@Aspect
@Configuration
public class LogAspect {
	private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);

//	private static final SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	
	// 定义切点 Pointcut
    @Pointcut("execution(* com.wenbronk.demo.controller.*Controller.*(..))")
    public void excudeController() {}
    
    @Pointcut("execution(* com.wenbronk.demo.tools.BaseInterfaceUtil.*(..))")
    public void excudeRestTemplate() {}
    
    @Around("excudeRestTemplate()")
    public Object doRest(ProceedingJoinPoint pjp) throws Throwable {
    	// Object[] args = pjp.getArgs();
		// SourceLocation sourceLocation = pjp.getSourceLocation();
		MethodSignature signature = (MethodSignature) pjp.getSignature();
		Method method = signature.getMethod();
		// Parameter[] parameters = method.getParameters();
		Long startTime = System.currentTimeMillis();
		Object result = pjp.proceed();
		LOGGER.debug("50-01 {} request {} {} {}", DateUtils.getDateTime(), pjp.getArgs()[0], "接口调用结束",
				"method: " + method.getName() + ", 耗时: " + (System.currentTimeMillis() - startTime) + "ms");
		LOGGER.info("50-01 {} request {} {} {}", DateUtils.getDateTime(), pjp.getArgs()[0],
				"method: " + method.getName() + ", 耗时: " + (System.currentTimeMillis() - startTime) + "ms", result);
		return result;
    }
    
    @Around("excudeController()")
    public Object doController(ProceedingJoinPoint pjp) throws Throwable {
		Long startTime = System.currentTimeMillis();

		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		ServletRequestAttributes sra = (ServletRequestAttributes) ra;
		HttpServletRequest request = sra.getRequest();

		String url = request.getRequestURL().toString();
		// String method = request.getMethod();
		// String uri = request.getRequestURI();
		String queryString = request.getQueryString();
		// LOGGER.info("请求开始, url: {}, method: {}, uri: {}, params: {}", url,
		// method, uri, queryString);
		LOGGER.info("50-01 {} status {} {} {}", DateUtils.getDateTime(), request.getParameter("userId"),
				url + "?" + queryString, "请求开始");
		// result的值就是被拦截方法的返回值
		Object result = pjp.proceed();
		// LOGGER.info("请求结束，url: {}, response: {}, time: {}s", url,
		// JSON.toJSONString(result), (System.currentTimeMillis() - startTime) /
		// 1000);
		LOGGER.info("50-01 {} status {} {} {}", DateUtils.getDateTime(), request.getParameter("userId"),
				"请求结束, 耗时" + (System.currentTimeMillis() - startTime) + "ms", url + "?" + queryString
				+ "--result: " + JSON.toJSONString(result));
		return result;
    }
	
}
