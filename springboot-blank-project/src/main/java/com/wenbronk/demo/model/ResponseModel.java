package com.wenbronk.demo.model;

import java.io.Serializable;

import com.wenbronk.demo.enums.PublicRespEnum;

/**
 * 响应模型
 * @params server_status  		200, 500, 404, 302, 306, etc.
 * 		   info 		 		the infomation of the server_status
 * 		   data					return data
 * @author wenbronk
 * @time 2017年4月14日  下午2:10:47  2017
 */
public class ResponseModel implements Serializable {
	private static final long serialVersionUID = 699897448645587050L;
	
	private String server_status;
	private String info;
	private Object data;
	
	public String getServer_status() {
		return server_status;
	}
	public void setServer_status(String server_status) {
		this.server_status = server_status;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	public static ResponseModel getSuccessModel() {
		return getModel(PublicRespEnum.RESCODE_200);
	}
	
	public static ResponseModel getModel(PublicRespEnum pe) {
		ResponseModel respModel = new ResponseModel();
		respModel.setServer_status(pe.getServer_status());
		respModel.setInfo(pe.getInfo());
		return respModel;
	}
	public static ResponseModel getModel(PublicRespEnum pe, String info) {
		ResponseModel respModel = new ResponseModel();
		respModel.setServer_status(pe.getServer_status());
		respModel.setInfo(info);
		return respModel;
	}
	public static ResponseModel getModel(PublicRespEnum pe, String info, Object data) {
		ResponseModel respModel = new ResponseModel();
		respModel.setServer_status(pe.getServer_status());
		respModel.setInfo(info);
		respModel.data = data;
		return respModel;
	}
	@Override
	public String toString(){
		return "{\"code\":"+this.server_status+",\"errorDescription\":\""+this.info+"\",\"dataObject\":\""+this.data+"\"}";
	}
	
}
