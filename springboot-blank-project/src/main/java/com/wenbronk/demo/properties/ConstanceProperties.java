package com.wenbronk.demo.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 常量配置
 * @author wenbronk
 * @Date 下午5:15:32
 */
@ConfigurationProperties(prefix = "custom.constance")
//@PropertySource("classpath:constance.properties")
public class ConstanceProperties {
	public static final Integer REDIS_CACHE_MAX = 10;

	private Integer cachemax = REDIS_CACHE_MAX;

	public Integer getCachemax() {
		return cachemax;
	}

	public void setCachemax(Integer cachemax) {
		this.cachemax = cachemax;
	}

}