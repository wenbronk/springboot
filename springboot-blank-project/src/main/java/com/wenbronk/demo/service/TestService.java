package com.wenbronk.demo.service;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;


@Service
public class TestService {

	@Inject
	private StringRedisTemplate redisTemplate;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public String deal(String name) {
		BoundValueOperations<String,String> boundValueOps = redisTemplate.boundValueOps("abc");
		boundValueOps.set("123");
		String string = boundValueOps.get();
		System.out.println(string);
		
		DBCollection collection = mongoTemplate.getCollection("user_info");
		long count = collection.count(new BasicDBObject().append("name", new BasicDBObject().put("$ne", null)));
		System.out.println(count);
		return name;
	}
	
}
