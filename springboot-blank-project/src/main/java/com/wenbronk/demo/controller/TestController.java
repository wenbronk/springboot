package com.wenbronk.demo.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wenbronk.demo.entity.UserEntity;
import com.wenbronk.demo.service.TestService;
import com.wenbronk.demo.tools.ConstanceUtils;

@RestController
public class TestController {
	
	@Resource
	private TestService testService;
	
	@Autowired
	private ConstanceUtils constanceUtils;

	@GetMapping("/demo/{name}")
	public String test(@PathVariable String name) {
		System.out.println("controller::::" + name);
		System.out.println(constanceUtils.getCachemax());
		return testService.deal(name);
	}
	
	@PostMapping("/post")
	public UserEntity test2(@RequestBody UserEntity user) {
		System.out.println(user);
		return user;
	}
	
}
