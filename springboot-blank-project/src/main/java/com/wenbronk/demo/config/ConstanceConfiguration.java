package com.wenbronk.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.wenbronk.demo.properties.ConstanceProperties;
import com.wenbronk.demo.tools.ConstanceUtils;


/**
 * 常量  java  config
 * @author wenbronk
 * @Date 下午5:18:54
 */
@Configuration
@EnableConfigurationProperties(ConstanceProperties.class)       // 声明开启属性注入, 通过 @Autowired注入
@ConditionalOnClass(ConstanceProperties.class)
@ConditionalOnProperty(prefix = "custom.constance", value = "enabled", matchIfMissing = true)
public class ConstanceConfiguration {
	
	@Autowired
	private ConstanceProperties contanceProperties;
	
	@Bean
	@ConditionalOnMissingBean(ConstanceUtils.class)
	public ConstanceUtils constanceUtils() {
		ConstanceUtils constanceUtils = new ConstanceUtils();
		constanceUtils.setCachemax(contanceProperties.getCachemax());
		return constanceUtils;
	}
	
	
}
