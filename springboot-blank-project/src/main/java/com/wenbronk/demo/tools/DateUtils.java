package com.wenbronk.demo.tools;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间值
 * @author wenbronk
 * @Date 下午4:53:58
 */
public class DateUtils {

	private static final DateFormat DATE_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static final DateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");
	
	public static Long getTimeMillions() {
		return System.currentTimeMillis();
	}
	
	public static String getDateTime() {
		return DATE_TIME.format(new Date());
	}
	public static String getDateTime(Long millions) {
		return DATE_TIME.format(millions);
	}
	
	public static String getDate() {
		return DATE.format(new Date());
	}
	public static String getDate(Long millions) {
		return DATE.format(millions);
	}
	
	public static String getTime(String prefix) {
		DateFormat format = new SimpleDateFormat(prefix);
		return format.format(new Date());
	}
	public static String getTime(String prefix, Long millions) {
		DateFormat format = new SimpleDateFormat(prefix);
		return format.format(new Date(millions));
	}
	
}
