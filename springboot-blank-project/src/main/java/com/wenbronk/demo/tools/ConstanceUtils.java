package com.wenbronk.demo.tools;

/**
 * 常量工具类
 * @author wenbronk
 * @Date 下午5:09:59
 */
public class ConstanceUtils {
	
	private Integer cachemax;

	public Integer getCachemax() {
		return cachemax;
	}

	public void setCachemax(Integer cachemax) {
		this.cachemax = cachemax;
	}
}
