package com.wenbronk.demo.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wenbronk.demo.tools.BaseInterfaceUtil;

/**
 * 测试restTemplate
 * @author wenbronk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class RestTemplateTest {

	@Autowired
	private BaseInterfaceUtil baseInterfaceUtil;
	
	/**
	 * 测试post提交
	 */
	@Test
	public void test1() {
		String json = "{" +
							"\"task_id\": \"poijgasdf\"," +
							"\"poi_ids\": [" +
										"{" +
											"\"geoNum\": \"32153512\"," + 
											"\"poiIds\": [" +
														"\"eqwogad#*#088998\"," + 
														"\"ewqioga#789789\"" +
													"]" +
										"}," +
										"{" +
											"\"geoNum\": \"32153512\"," + 
											"\"poiIds\": [" +
														"\"eqwogad\"," + 
														"\"ewqiogasd\"" +
													"]" +
										"}" +
									"]" +
						"}";
		JSONObject params = JSON.parseObject(json);
		String url = "http://localhost:8010/collect-data-scrapy/webServe/reportErrorPois";
		JSONObject jsonObject = baseInterfaceUtil.requestPost(url, params);
		System.out.println(jsonObject);
	}
	
	@Test
	public void test2() {
		System.out.println("hello");
	}
	
}
