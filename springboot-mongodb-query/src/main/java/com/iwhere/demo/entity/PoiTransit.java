package com.iwhere.demo.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="poiTransit")
public class PoiTransit {
	
//	@Indexed(unique = true)
	@Id
	private String _id;
	private String name;
	private Integer geolevel;
	private String geo_num;
	private Date create_time;
	private Date modified_time;
	private List<String> type;
	private String typecode;
	private String biz_type;
	private String address;
	private LocationEntity location;
	private List<String> tel;
	private String pcode;
	private String pname;
	private String citycode;
	private String cityname;
	private String adcode;
	private String adname;
	private String gridcode;
	private String navi_poiid;
	private LocationEntity entr_location;
	private Double match;
	private Double recommend;
	private String indoor_map;
	private IndoorData indoor_data;
	private String groupbuy_num;
	private String discount_num;
	private BizExt biz_ext;
	private List<DeepInfo> deep_info;
	
	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getGeolevel() {
		return geolevel;
	}

	public void setGeolevel(Integer geolevel) {
		this.geolevel = geolevel;
	}

	public String getGeo_num() {
		return geo_num;
	}

	public void setGeo_num(String geo_num) {
		this.geo_num = geo_num;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getModified_time() {
		return modified_time;
	}

	public void setModified_time(Date modified_time) {
		this.modified_time = modified_time;
	}

	public List<String> getType() {
		return type;
	}

	public void setType(List<String> type) {
		this.type = type;
	}

	public String getTypecode() {
		return typecode;
	}

	public void setTypecode(String typecode) {
		this.typecode = typecode;
	}

	public String getBiz_type() {
		return biz_type;
	}

	public void setBiz_type(String biz_type) {
		this.biz_type = biz_type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LocationEntity getLocation() {
		return location;
	}

	public void setLocation(LocationEntity location) {
		this.location = location;
	}

	public List<String> getTel() {
		return tel;
	}

	public void setTel(List<String> tel) {
		this.tel = tel;
	}

	public String getPcode() {
		return pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getCityname() {
		return cityname;
	}

	public void setCityname(String cityname) {
		this.cityname = cityname;
	}

	public String getAdcode() {
		return adcode;
	}

	public void setAdcode(String adcode) {
		this.adcode = adcode;
	}

	public String getAdname() {
		return adname;
	}

	public void setAdname(String adname) {
		this.adname = adname;
	}

	public String getGridcode() {
		return gridcode;
	}

	public void setGridcode(String gridcode) {
		this.gridcode = gridcode;
	}

	public String getNavi_poiid() {
		return navi_poiid;
	}

	public void setNavi_poiid(String navi_poiid) {
		this.navi_poiid = navi_poiid;
	}

	public LocationEntity getEntr_location() {
		return entr_location;
	}

	public void setEntr_location(LocationEntity entr_location) {
		this.entr_location = entr_location;
	}

	public Double getMatch() {
		return match;
	}

	public void setMatch(Double match) {
		this.match = match;
	}

	public Double getRecommend() {
		return recommend;
	}

	public void setRecommend(Double recommend) {
		this.recommend = recommend;
	}

	public String getIndoor_map() {
		return indoor_map;
	}

	public void setIndoor_map(String indoor_map) {
		this.indoor_map = indoor_map;
	}

	public IndoorData getIndoor_data() {
		return indoor_data;
	}

	public void setIndoor_data(IndoorData indoor_data) {
		this.indoor_data = indoor_data;
	}

	public String getGroupbuy_num() {
		return groupbuy_num;
	}

	public void setGroupbuy_num(String groupbuy_num) {
		this.groupbuy_num = groupbuy_num;
	}

	public String getDiscount_num() {
		return discount_num;
	}

	public void setDiscount_num(String discount_num) {
		this.discount_num = discount_num;
	}

	public BizExt getBiz_ext() {
		return biz_ext;
	}

	public void setBiz_ext(BizExt biz_ext) {
		this.biz_ext = biz_ext;
	}

	public List<DeepInfo> getDeep_info() {
		return deep_info;
	}

	public void setDeep_info(List<DeepInfo> deep_info) {
		this.deep_info = deep_info;
	}

	
	//*********** inner class
	public class IndoorData {
		
	}
	
	public class BizExt {
		private Double rating;

		public Double getRating() {
			return rating;
		}

		public void setRating(Double rating) {
			this.rating = rating;
		}
	}
	
	public class DeepInfo {
		private String deep_info_type;
		private String source_type;
		private String source_url;
		private String name;
		private List<String> urls;
		private List<String> type;
		private String address;
		private List<String> tel;
		public String getDeep_info_type() {
			return deep_info_type;
		}
		public void setDeep_info_type(String deep_info_type) {
			this.deep_info_type = deep_info_type;
		}
		public String getSource_type() {
			return source_type;
		}
		public void setSource_type(String source_type) {
			this.source_type = source_type;
		}
		public String getSource_url() {
			return source_url;
		}
		public void setSource_url(String source_url) {
			this.source_url = source_url;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public List<String> getUrls() {
			return urls;
		}
		public void setUrls(List<String> urls) {
			this.urls = urls;
		}
		public List<String> getType() {
			return type;
		}
		public void setType(List<String> type) {
			this.type = type;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public List<String> getTel() {
			return tel;
		}
		public void setTel(List<String> tel) {
			this.tel = tel;
		}
	}
}
