package com.iwhere.demo.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iwhere.demo.enums.PublicRespEnum;
import com.iwhere.demo.model.ResponseModel;

@ControllerAdvice
public class MyException {
	private static final Logger LOGGER = LoggerFactory.getLogger(MyException.class);

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseModel defaultHandler(HttpServletRequest request, Exception exception) {
		StringBuffer url = request.getRequestURL();
		String params = request.getQueryString();
		// 发生地点
		int lineNum = 0;
		String className = null;
		String methodName = null;
		StackTraceElement[] st = exception.getStackTrace();
		if (ArrayUtils.isNotEmpty(st)) {
			lineNum = st[0].getLineNumber();
			className = st[0].getClassName();
			methodName = st[0].getMethodName();
		}

		String message = exception.getMessage();
		LOGGER.error("发生异常: {}#{}() 在第{}行发生{}异常!!! url: {}, params: {}, info: {}", className, methodName, lineNum,
				exception.getClass().getName(), url, params, message);
		// exception.printStackTrace();
		return ResponseModel.getModel(PublicRespEnum.RESCODE_500, message);
	}

	/**
	 * 绑定request中的参数
	 * 
	 * @param binder
	 * @Date 下午2:38:54
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("id");
	}

}
