package com.iwhere.demo.tools;

/**
 * 常量工具类
 * @author wenbronk
 * @Date 下午5:09:59
 */
public class ConstanceUtils {
	
	private Integer page = 1;
	private Integer pageSize;
	private Integer pageStart;
	
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageStart() {
		if (pageStart == null || pageStart.compareTo(0) == 0) {
			this.pageStart = (page - 1) * pageSize;
		}
		return pageStart;
	}
	public void setPageStart(Integer pageStart) {
		this.pageStart = pageStart;
	}
}
