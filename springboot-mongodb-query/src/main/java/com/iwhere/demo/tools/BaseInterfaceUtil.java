package com.iwhere.demo.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONObject;

/**
 * geo-sot基础接口工具类
 * @author wenbronk
 * @time 2017年4月17日 下午3:42:39 2017
 */
@Component
@ConfigurationProperties(prefix = "interface.server.url")
public class BaseInterfaceUtil {
	private static Logger LOGGER = LoggerFactory.getLogger(BaseInterfaceUtil.class);

	private String geosot;
	
	@Autowired
	private RestTemplate restTemplate;

	public JSONObject requestUrl(String name) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(geosot).append("/getUserByName/").append(name);
		LOGGER.info("url请求: {}", stringBuilder.toString());
		return restTemplate.getForObject(stringBuilder.toString(), JSONObject.class);
	}
	

	public String getGeosot() {
		return geosot;
	}

	public void setGeosot(String geosot) {
		this.geosot = geosot;
	}
}
