package com.iwhere.demo.tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTools {

	private static final DateFormat DATE_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static String getDateTime() {
		return DATE_TIME.format(new Date());
	}
	public static Long getDateTime(String dateTime) throws ParseException {
		return DATE_TIME.parse(dateTime).getTime();
	}
	public static String getDateTime(Long timemillions) {
		return DATE_TIME.format(new Date(timemillions));
	}
	
	private static final DateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");
	public static String getDate() {
		return DATE.format(new Date());
	}
	public static Long getDate(String dateTime) throws ParseException {
		return DATE.parse(dateTime).getTime();
	}
	public static String getDate(Long timemillions) {
		return DATE.format(new Date(timemillions));
	}
	
}
