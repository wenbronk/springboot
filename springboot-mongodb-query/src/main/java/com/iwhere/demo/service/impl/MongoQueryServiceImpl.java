package com.iwhere.demo.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.iwhere.demo.entity.PoiTransit;
import com.iwhere.demo.service.MongoQueryService;
import com.iwhere.demo.tools.ConstanceUtils;;

@Service
public class MongoQueryServiceImpl implements MongoQueryService {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private ConstanceUtils constanceUtils;

	@Override
	public List<PoiTransit> findByIn(String[] idArray) {
		Criteria criteria = Criteria.where("_id").in(idArray);
		List<PoiTransit> list = mongoTemplate.find(Query.query(criteria), PoiTransit.class);
		return list;
	}
	
	@Override
	public PoiTransit updatePoi(String poiId, String iwhereintroduce) {
		PoiTransit transit = mongoTemplate.findAndModify(Query.query(Criteria.where("_id").is(poiId)), 
				new Update().set("iwhereintroduce", iwhereintroduce), PoiTransit.class);
		return transit;
	}

	@Override
	public Long countByTime(String filedName, String minTime, String maxTime) throws ParseException {
		Criteria criteria = buildCriteria(filedName,  minTime, maxTime);
		Long count = mongoTemplate.count(Query.query(criteria), PoiTransit.class);
		return count;
	}

	
	@Override
	public List<PoiTransit> findByTime(String filedName, String minTime, String maxTime) throws ParseException {
		Criteria criteria = buildCriteria(filedName,  minTime, maxTime);
		List<PoiTransit> list = mongoTemplate.find(Query.query(criteria), PoiTransit.class);
		return list;
	}
	
	@Override
	public Long countByExample(PoiTransit baseEntity)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Criteria buildCriteria = buildCriteria(baseEntity, PoiTransit.class);
		Long count = mongoTemplate.count(Query.query(buildCriteria), PoiTransit.class);
		return count;
	}
	
	/**
	 * 加入保护性分页
	 */
	@Override
	public List<PoiTransit> findByExample(PoiTransit baseEntity) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Criteria buildCriteria = buildCriteria(baseEntity, PoiTransit.class);
		@SuppressWarnings("deprecation")
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(buildCriteria),
				Aggregation.skip(constanceUtils.getPage()),
				Aggregation.limit(constanceUtils.getPageSize()));
		AggregationResults<PoiTransit> aggRes = mongoTemplate.aggregate(aggregation, "poiTransit",
				PoiTransit.class);
		List<PoiTransit> listRes = aggRes.getMappedResults();
		return listRes;
	}
}
