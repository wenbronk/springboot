package com.iwhere.demo.service;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.util.Assert;

import com.iwhere.demo.entity.PoiTransit;
import com.iwhere.demo.tools.DateTools;

public interface MongoQueryService {
	
	List<PoiTransit> findByExample(PoiTransit baseEntity)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;
	
	Long countByExample(PoiTransit baseEntity)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;
	
	List<PoiTransit> findByTime(String filedName, String minTime, String maxTime) throws ParseException;
	
	Long countByTime(String filedName, String minTime, String maxTime) throws ParseException;
	
	List<PoiTransit> findByIn(String[] idArray);
	
	default Criteria buildCriteria(String filedName, String minTime, String maxTime) throws ParseException {
		ArrayList<Criteria> list = new ArrayList<Criteria>();
		if (StringUtils.isNotBlank(minTime)) {
			list.add(where(filedName).gte(DateTools.getDateTime(minTime)));
		}
		if (StringUtils.isNotBlank(maxTime)) {
			list.add(where(filedName).lte(DateTools.getDateTime(maxTime)));
		}
		Assert.notEmpty(list, "Please enter a valid parameter");
		Criteria criteria = new Criteria();
		criteria.andOperator(list.toArray(new Criteria[0]));
		return criteria;
	}
	
	default <T> Criteria buildCriteria(T t, Class<T> clazz)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<Criteria> list = new ArrayList<>();
		for (Method method : clazz.getMethods()) {
			String name = method.getName();
			Object value = null;
			if (name.startsWith("get")) {
				value = method.invoke(t);
			}
			if (value != null && !"".equals(value)) {
				if (name.endsWith("Class")) {
					continue;
				}
				String field = name.substring(3).toLowerCase();
				Criteria criteria = Criteria.where(field).is(value);
				list.add(criteria);
			}
		}
		Criteria criteria = new Criteria();
		if (list != null && list.size() != 0) {
			criteria.andOperator(list.toArray(new Criteria[0]));
		}
		return criteria;
	}

	PoiTransit updatePoi(String poiId, String iwhereintroduce);

}
