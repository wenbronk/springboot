package com.iwhere.demo.controller;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iwhere.demo.entity.PoiTransit;
import com.iwhere.demo.enums.PublicRespEnum;
import com.iwhere.demo.model.ResponseModel;
import com.iwhere.demo.service.MongoQueryService;

@RestController
public class MongoController {

	@Autowired
	private MongoQueryService mongoQueryService;
	
	@RequestMapping("/findByIn/{ids}")
	public ResponseModel findByIn(@PathVariable("ids") String ids) {
		String[] idArray = ids.split(", *");
		List<PoiTransit> list = mongoQueryService.findByIn(idArray);
		return ResponseModel.getModel(PublicRespEnum.RESCODE_200, String.valueOf(list.size()), list);
	}
	
	@RequestMapping("/updatePoi")
	public ResponseModel updatePoi(String poiId, String iwhereintroduce) {
		PoiTransit transit = mongoQueryService.updatePoi(poiId, iwhereintroduce);
		return ResponseModel.getModel(PublicRespEnum.RESCODE_200, "success", transit);
	}
	
	@GetMapping("/countByTime/{filedName}/{gteTime}/{lteTime}")
	public ResponseModel countByTime(@PathVariable("filedName") String filedName,
			@PathVariable("gteTime") String minTime, @PathVariable("lteTime") String maxTime) throws ParseException {
		Assert.notNull(filedName, "fileName not be null");
		Long countByTime = mongoQueryService.countByTime(filedName, minTime, maxTime);
		return ResponseModel.getModel(PublicRespEnum.RESCODE_200, "success", countByTime);
	}

	@GetMapping("/findByTime/{filedName}/{gteTime}/{lteTime}")
	public ResponseModel findByTime(@PathVariable("filedName") String filedName,
			@PathVariable("gteTime") String minTime, @PathVariable("lteTime") String maxTime) throws ParseException {
		Assert.notNull(filedName, "fileName not be null");
		List<PoiTransit> list = mongoQueryService.findByTime(filedName, minTime, maxTime);
		return ResponseModel.getModel(PublicRespEnum.RESCODE_200, "success", list);
	}

	@RequestMapping("/findByExample")
	public ResponseModel findByExample(PoiTransit baseEntity)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<PoiTransit> examples = mongoQueryService.findByExample(baseEntity);
		return ResponseModel.getModel(PublicRespEnum.RESCODE_200, "success", examples);
	}

}
