package com.iwhere.demo.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 常量配置
 * 
 * @author wenbronk
 * @Date 下午5:15:32
 */
@ConfigurationProperties(prefix = "custom.constance")
public class ConstanceProperties {
	public static final Integer PAGE = 0;
	public static final Integer PAGE_SIZE = 100;

	private Integer page = PAGE;
	private Integer pageSize = PAGE_SIZE;
	
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
