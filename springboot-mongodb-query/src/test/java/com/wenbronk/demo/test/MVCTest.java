package com.wenbronk.demo.test;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.iwhere.demo.MongoApp;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MongoApp.class)
public class MVCTest {

	private MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext wac;

	@Test
	public void test1() {
//		String responseString = mockMvc.perform(
//                get("/categories/getAllCategory")    //请求的url,请求的方法是get
//                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)  //数据的格式
//                        .param("pcode","root")         //添加参数
//        ).andExpect(status().isOk())    //返回的状态是200
//                .andDo(print())         //打印出请求和相应的内容
//                .andReturn().getResponse().getContentAsString();   //将相应的数据转换为字符串
        System.out.println("--------返回的json = " + mockMvc );
	}

	@Before
	public void before() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(wac).build();
	}

}
