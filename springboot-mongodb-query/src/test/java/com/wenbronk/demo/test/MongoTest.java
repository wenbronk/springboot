package com.wenbronk.demo.test;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.iwhere.demo.MongoApp;
import com.iwhere.demo.entity.PoiTransit;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {MongoApp.class})
public class MongoTest {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Test
	public void test2() {
		Criteria criteria = new Criteria();
		criteria.andOperator();
//		List<PoiTransit> find = mongoTemplate.find(new Query().addCriteria(criteria), PoiTransit.class);
		long count = mongoTemplate.count(new Query(criteria), PoiTransit.class);
		System.out.println("||||||||||||||" + count);
	}
	
	@Test
	public void test1() {
		Set<String> collectionNames = mongoTemplate.getCollectionNames();
		for (String string : collectionNames) {
			System.out.println(string);
		}
	}

}
