package com.wenbronk.websocket.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wenbronk.websocket.service.impl.MyWebSocket;

@RestController
public class TestController {

	@Autowired
	private MyWebSocket myWebSocket;
	
	@RequestMapping("/abc/{message}")
	public void test(@PathVariable String message) {
		System.out.println("success: " + message);
		try {
			myWebSocket.sendMessage("succes", "123");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

