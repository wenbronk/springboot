package com.wenbronk.websocket2.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RestController;

import com.wenbronk.websocket2.wisely.WiselyMessage;
import com.wenbronk.websocket2.wisely.WiselyResponse;

//@RestController
public class WsController {
	
	/**
	 * @MessageMapping和@RequestMapping功能类似，用于设置URL映射地址，浏览器向服务器发起请求，需要通过该地址。
	 * @param message
	 * @return
	 * @throws Exception
	 */
	@MessageMapping("/welcome")
	/**
	 * 如果服务器接受到了消息，就会对订阅了@SendTo括号中的地址传送消息。
	 */
	@SendTo("/topic/getResponse")
	public WiselyResponse say(WiselyMessage message) throws Exception {
		return new WiselyResponse("Welcome, " + message.getName() + "!");
	}

}
