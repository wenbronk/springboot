package com.iwhere.test.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 在普通类中引入spring中的bean, 在springboot的扫描包下
 * @ServletComponentScan
 * 
 * 如果不在springboot的包扫描下, 则不需要component, 但需要在App.java中引入
 * @Import(value={SpringAppUtils.class})
 * 
 * @author 231
 * @time 2017年3月15日 下午2:40:32 2017
 */
@Component
public class SpringAppUtils implements ApplicationContextAware {
	private static Logger LOGGER = LoggerFactory.getLogger(SpringAppUtils.class);
	private static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		if (SpringAppUtils.applicationContext == null) {
			applicationContext = context;
		}
		LOGGER.info("springAppUtils is init");
	}

	/**
	 * 获取applicationContext
	 * 
	 * @return
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 通过那么获取bean
	 */
	public static Object getBean(String name) {
		return getApplicationContext().getBean(name);
	}
	
	/**
	 * 通过class获取bean
	 */
	public static <T> T getBean(Class<T> clazz) {
		return getApplicationContext().getBean(clazz);
	}
	
	/**
	 * 通过name和class获取bean
	 */
	public static <T> T getBean(String name, Class<T> clazz) {
		return getApplicationContext().getBean(name, clazz);
	}
}
