package com.iwhere.test.dao;

import org.springframework.data.repository.CrudRepository;

import com.iwhere.test.demo.Demo;

/**
 * 使用crud接口的dao层
 * @author 231
 *
 */
public interface DemoRepository extends CrudRepository<Demo, Long> {

}
