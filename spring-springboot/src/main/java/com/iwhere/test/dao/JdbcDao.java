package com.iwhere.test.dao;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.iwhere.test.demo.Demo;

/**
 * 使用jdbcTemplate操作数据库
 * @author 231
 * 
 */
@Repository
public class JdbcDao {

	@Resource
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * 通过id获取对象
	 * @return
	 */
	public Demo getById(Integer id) {
		String sql = "select * from demo where id = ?";
		BeanPropertyRowMapper<Demo> mapper = new BeanPropertyRowMapper<Demo>(Demo.class);
		return jdbcTemplate.queryForObject(sql, mapper, id);
	}
	
}
