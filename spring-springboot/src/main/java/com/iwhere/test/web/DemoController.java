package com.iwhere.test.web;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iwhere.test.demo.Demo;
import com.iwhere.test.service.DemoService;
import com.iwhere.test.service.JdbcService;

@RestController
@RequestMapping("/demo")
public class DemoController {

	@Resource
	private DemoService demoService;
	
	@Resource
	private JdbcService jdbcService;
	
	/**
	 * 返回json格式数据
	 * @RestController
	 * @return
	 */
	@RequestMapping("/getDemo")
	public Demo getDemo() {
		Demo demo = new Demo();
		demo.setId(1);
		demo.setName("Angel");
		return demo;
	}
	
	/**
	 * 测试jpa的使用
	 * @return
	 */
	@RequestMapping("/save")
	public String saveDemo() {
		Demo demo = new Demo();
		demo.setId(1);
		demo.setName("Angel");
		demo.setSex("female");
		demoService.save(demo);
		return "ok";
	}
	
	/**
	 * 测试repository
	 * http://localhost:8080/demo/findOne?id=1
	 * @param id
	 * @return
	 */
	@RequestMapping("/findOne")
	public Demo findOne(long id) {
		return demoService.findOne(id);
	}
	
	/**
	 * 测试jdbcTemplate使用
	 * http://localhost:8080/demo/getById?id=1
	 * @param id
	 * @return
	 */
	@RequestMapping("/getById")
	public Demo getDemoById(Integer id) {
		return jdbcService.getById(id);
	}
	
//	@RequestMapping("/zeroExcetpion")
//	public int zeroException() {
//		System.err.println("zero");
//		return 1/0;
//	}
}
