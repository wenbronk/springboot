package com.iwhere.test.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller
 * @author 231
 */
@RestController
public class HelloController {

	@RequestMapping("/")
	public String hello( ){
		return "hello world";
	}
	
}
