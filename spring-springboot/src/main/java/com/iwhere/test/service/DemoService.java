package com.iwhere.test.service;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.iwhere.test.dao.DemoRepository;
import com.iwhere.test.demo.Demo;

/**
 * service层
 * @author 231
 */
@Service
public class DemoService {

	@Resource
	private DemoRepository demoRepository;
	
	@Transactional
	public void save(Demo demo)	 {
		demoRepository.save(demo);
	}
	
	public Demo findOne(long id) {
		Demo demo = demoRepository.findOne(id);
		return demo;
	}
	
}
