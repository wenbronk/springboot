package com.iwhere.test.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.iwhere.test.dao.JdbcDao;
import com.iwhere.test.demo.Demo;

/**
 * 使用jdbcTemplate
 * @author 231
 */
@Service
public class JdbcService {

	@Resource
	private JdbcDao jdbcDao;
	
	public Demo getById(Integer id) {
		return jdbcDao.getById(id);
	}
	
}
