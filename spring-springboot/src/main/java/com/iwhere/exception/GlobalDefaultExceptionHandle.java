package com.iwhere.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 全局异常处理
 * @author 231
 *
 */

@ControllerAdvice
public class GlobalDefaultExceptionHandle {

	
	/**
	 * 可通过modelandView返回试图
	 * 需要返回json的话, 加@ResponseBody即可
	 * @param req
	 * @param e
	 */
	@ExceptionHandler(value=Exception.class)
	public void defalutErrorHandler(HttpServletRequest req, Exception e) {
		e.printStackTrace();
		System.out.println("my exception handler");
	}
}
