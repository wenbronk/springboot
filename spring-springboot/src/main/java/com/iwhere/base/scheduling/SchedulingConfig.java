package com.iwhere.base.scheduling;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 整合定时任务
 * @author 231
 *
 */
@Configuration		// 相当于配置beans, 
@EnableScheduling	// <task:*>, 让spring进行任务调度
public class SchedulingConfig {

	@Scheduled(cron="0/20 * * * * ?")	// 20秒执行一次
	public void scheduler() {
		System.out.println("success for Scehduler");
	}
	
}
