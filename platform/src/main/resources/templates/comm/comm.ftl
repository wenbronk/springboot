<#assign base=request.contextPath />

<!-- 基础 css -->
<link rel="stylesheet" href="${base}/static/comm/css/comm.css">
<!-- jquery -->
<script type="text/javascript" src="${base}/webjars/jquery/2.2.4/jquery.min.js"></script>

<!-- 基础JavaScript -->
<script type="text/javascript">
    var base = "${base}";
</script>