<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <#include "../comm/comm.ftl">
</head>
<body>
    user:${count}

    <p id="test"></p>

    <script type="text/javascript">
        $(function () {
            $.post(base + "/order/api/getUserName", {
                userId: 10
            }, function (resp) {
                if (resp.server_status == 200){
                    $("#test").text(resp.userId + resp.userName);
                }
            });
        });
    </script>
</body>
</html>