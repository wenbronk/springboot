package com.iwhere.platform.biz.order.service.impl;

import com.iwhere.platform.biz.order.dao.mybatis.TestDao;
import com.iwhere.platform.biz.order.service.ITestService;
import com.iwhere.platform.comm.dao.mongodb.ClientRepository;
import com.iwhere.platform.comm.dao.mongodb.entity.ClientEntity;
import com.iwhere.platform.comm.dao.mybatis.UserMapper;
import com.iwhere.platform.comm.dao.mybatis.example.UserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/9/20 15:44
 * ---------------------------------------------------------------------------
 */
@Service
public class TestServiceImpl implements ITestService {
    @Autowired
    private TestDao testDao;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ClientRepository clientRepository;

    /**
     * test
     * @return
     */
    @Override
    public long countUser() {
        long count1 = testDao.countUser();
        long count2 = userMapper.countByExample(
                new UserExample()
                        .createCriteria()
                        .andUserIdLessThan(100L)
                        .example()
        );
        clientRepository.save(
                ClientEntity.builder()
                        .count(count1)
                        .build()
        );
        return count1 - count2;
    }
}
