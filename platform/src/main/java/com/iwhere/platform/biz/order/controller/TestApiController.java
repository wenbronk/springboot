package com.iwhere.platform.biz.order.controller;

import com.iwhere.platform.biz.order.dto.req.GetUserNameReq;
import com.iwhere.platform.biz.order.dto.resp.GetUserNameResp;
import com.iwhere.platform.utils.base.dto.BaseResp;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/9/20 16:29
 * ---------------------------------------------------------------------------
 */
@RestController
@RequestMapping("order/api")
public class TestApiController {

    /**
     * 获取用户名
     * @param req
     * @return
     */
    @RequestMapping("getUserName")
    public BaseResp getUserName(@Validated GetUserNameReq req){
        return GetUserNameResp.builder()
                .userId(req.getUserId())
                .userName("test")
                .build();

        // ---
//        return BaseResp.map()
//                .append("userId", req.getUserId())
//                .append("userName", "test");
    }
}
