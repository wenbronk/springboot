package com.iwhere.platform.biz.order.controller;

import com.iwhere.platform.biz.order.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/9/20 14:49
 * ---------------------------------------------------------------------------
 */
@Controller
@RequestMapping("order")
public class TestViewController {
    @Autowired
    private ITestService testService;   // test

    /**
     * test
     * @return
     */
    @RequestMapping("test")
    public ModelAndView test(){
        Map map = new HashMap();
        map.put("count", testService.countUser());
        return new ModelAndView("order/test", map);
    }
}
