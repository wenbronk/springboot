package com.iwhere.platform.biz.order.dto.resp;

import com.iwhere.platform.utils.base.dto.BaseResp;
import lombok.Builder;
import lombok.Data;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/9/20 16:39
 * ---------------------------------------------------------------------------
 */
@Data
@Builder
public class GetUserNameResp extends BaseResp {
    private Long userId;    // 用户ID
    private String userName;    // 用户名
}
