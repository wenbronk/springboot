package com.iwhere.platform.biz.order.dao.mybatis;

import org.apache.ibatis.annotations.Param;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/9/20 15:40
 * ---------------------------------------------------------------------------
 */
public interface TestDao {
    /**
     * test
     * @return
     */
    long countUser();
}
