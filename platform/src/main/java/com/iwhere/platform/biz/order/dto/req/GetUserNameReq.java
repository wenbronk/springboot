package com.iwhere.platform.biz.order.dto.req;

import com.iwhere.platform.utils.base.dto.BaseReq;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/9/20 16:31
 * ---------------------------------------------------------------------------
 */
@Data
public class GetUserNameReq extends BaseReq {
    @NotNull
    @Min(1)
    private Long userId;
}
