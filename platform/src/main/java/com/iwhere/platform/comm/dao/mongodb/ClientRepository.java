package com.iwhere.platform.comm.dao.mongodb;

import com.iwhere.platform.comm.dao.mongodb.entity.ClientEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * ---------------------------------------------------------------------------
 * 客户端
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/30 20:39
 * ---------------------------------------------------------------------------
 */
public interface ClientRepository extends CrudRepository<ClientEntity, String> {
}
