package com.iwhere.platform.comm.dao.mybatis.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table trade_detail_tipping
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class TradeDetailTipping {
    /**
     * Database Column Remarks:
     *   商户订单号
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column trade_detail_tipping.trade_no
     *
     * @mbg.generated
     */
    private String tradeNo;

    /**
     * Database Column Remarks:
     *   被打赏者
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column trade_detail_tipping.to_user_id
     *
     * @mbg.generated
     */
    private Long toUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table trade_detail_tipping
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    private Map<String, Boolean> selectiveColumns = new HashMap<String, Boolean>();

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column trade_detail_tipping.trade_no
     *
     * @return the value of trade_detail_tipping.trade_no
     *
     * @mbg.generated
     */
    public String getTradeNo() {
        return tradeNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column trade_detail_tipping.trade_no
     *
     * @param tradeNo the value for trade_detail_tipping.trade_no
     *
     * @mbg.generated
     */
    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column trade_detail_tipping.to_user_id
     *
     * @return the value of trade_detail_tipping.to_user_id
     *
     * @mbg.generated
     */
    public Long getToUserId() {
        return toUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column trade_detail_tipping.to_user_id
     *
     * @param toUserId the value for trade_detail_tipping.to_user_id
     *
     * @mbg.generated
     */
    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trade_detail_tipping
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static TradeDetailTipping.Builder builder() {
        return new TradeDetailTipping.Builder();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trade_detail_tipping
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public boolean isSelective() {
        return this.selectiveColumns.size() > 0;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trade_detail_tipping
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public boolean isSelective(String column) {
        return this.selectiveColumns.get(column) != null;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trade_detail_tipping
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public TradeDetailTipping selective(Column ... columns) {
        this.selectiveColumns.clear();
        if (columns != null) {
            for (Column column : columns) {
                this.selectiveColumns.put(column.value(), true);
            }
        }
        return this;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table trade_detail_tipping
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static class Builder {
        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private TradeDetailTipping obj;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder() {
            this.obj = new TradeDetailTipping();
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column trade_detail_tipping.trade_no
         *
         * @param tradeNo the value for trade_detail_tipping.trade_no
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder tradeNo(String tradeNo) {
            obj.setTradeNo(tradeNo);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column trade_detail_tipping.to_user_id
         *
         * @param toUserId the value for trade_detail_tipping.to_user_id
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder toUserId(Long toUserId) {
            obj.setToUserId(toUserId);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public TradeDetailTipping build() {
            return this.obj;
        }
    }

    /**
     * This enum was generated by MyBatis Generator.
     * This enum corresponds to the database table trade_detail_tipping
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public enum Column {
        tradeNo("trade_no"),
        toUserId("to_user_id");

        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private final String column;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String value() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String getValue() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        Column(String column) {
            this.column = column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String desc() {
            return this.column + " DESC";
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_tipping
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String asc() {
            return this.column + " ASC";
        }
    }
}