package com.iwhere.platform.comm.dao.mybatis.model;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table team_permission
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class TeamPermissionKey {
    /**
     * Database Column Remarks:
     *   团队编号
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_permission.team_num
     *
     * @mbg.generated
     */
    private String teamNum;

    /**
     * Database Column Remarks:
     *   用户ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_permission.user_id
     *
     * @mbg.generated
     */
    private Long userId;

    /**
     * Database Column Remarks:
     *   权限（1:对讲-唯一、2：设置集合、3：设置安全边界、4：移除团员、5：结束行程）
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_permission.permission
     *
     * @mbg.generated
     */
    private Short permission;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_permission.team_num
     *
     * @return the value of team_permission.team_num
     *
     * @mbg.generated
     */
    public String getTeamNum() {
        return teamNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_permission.team_num
     *
     * @param teamNum the value for team_permission.team_num
     *
     * @mbg.generated
     */
    public void setTeamNum(String teamNum) {
        this.teamNum = teamNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_permission.user_id
     *
     * @return the value of team_permission.user_id
     *
     * @mbg.generated
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_permission.user_id
     *
     * @param userId the value for team_permission.user_id
     *
     * @mbg.generated
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_permission.permission
     *
     * @return the value of team_permission.permission
     *
     * @mbg.generated
     */
    public Short getPermission() {
        return permission;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_permission.permission
     *
     * @param permission the value for team_permission.permission
     *
     * @mbg.generated
     */
    public void setPermission(Short permission) {
        this.permission = permission;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_permission
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static TeamPermissionKey.Builder builder() {
        return new TeamPermissionKey.Builder();
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table team_permission
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static class Builder {
        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private TeamPermissionKey obj;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder() {
            this.obj = new TeamPermissionKey();
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_permission.team_num
         *
         * @param teamNum the value for team_permission.team_num
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder teamNum(String teamNum) {
            obj.setTeamNum(teamNum);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_permission.user_id
         *
         * @param userId the value for team_permission.user_id
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder userId(Long userId) {
            obj.setUserId(userId);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_permission.permission
         *
         * @param permission the value for team_permission.permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder permission(Short permission) {
            obj.setPermission(permission);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public TeamPermissionKey build() {
            return this.obj;
        }
    }

    /**
     * This enum was generated by MyBatis Generator.
     * This enum corresponds to the database table team_permission
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public enum Column {
        teamNum("team_num"),
        userId("user_id"),
        permission("permission");

        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private final String column;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String value() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String getValue() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        Column(String column) {
            this.column = column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String desc() {
            return this.column + " DESC";
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_permission
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String asc() {
            return this.column + " ASC";
        }
    }
}