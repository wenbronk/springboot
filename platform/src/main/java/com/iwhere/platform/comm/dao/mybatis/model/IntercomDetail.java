package com.iwhere.platform.comm.dao.mybatis.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table intercom_detail
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class IntercomDetail {
    /**
     * Database Column Remarks:
     *   对讲ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.intercom_id
     *
     * @mbg.generated
     */
    private Long intercomId;

    /**
     * Database Column Remarks:
     *   用户ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.user_id
     *
     * @mbg.generated
     */
    private Long userId;

    /**
     * Database Column Remarks:
     *   团队编号
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.team_num
     *
     * @mbg.generated
     */
    private String teamNum;

    /**
     * Database Column Remarks:
     *   对讲开始时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.start_time
     *
     * @mbg.generated
     */
    private Date startTime;

    /**
     * Database Column Remarks:
     *   对讲结束时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.end_time
     *
     * @mbg.generated
     */
    private Date endTime;

    /**
     * Database Column Remarks:
     *   免费对讲(0:不免费、1：免费)
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.free
     *
     * @mbg.generated
     */
    private Short free;

    /**
     * Database Column Remarks:
     *   计费标志位(0:未结束计费、1：已经结束计费)
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.count_flag
     *
     * @mbg.generated
     */
    private Short countFlag;

    /**
     * Database Column Remarks:
     *   对讲人数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.user_count
     *
     * @mbg.generated
     */
    private Integer userCount;

    /**
     * Database Column Remarks:
     *   消耗时长
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column intercom_detail.use_time
     *
     * @mbg.generated
     */
    private Float useTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table intercom_detail
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    private Map<String, Boolean> selectiveColumns = new HashMap<String, Boolean>();

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.intercom_id
     *
     * @return the value of intercom_detail.intercom_id
     *
     * @mbg.generated
     */
    public Long getIntercomId() {
        return intercomId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.intercom_id
     *
     * @param intercomId the value for intercom_detail.intercom_id
     *
     * @mbg.generated
     */
    public void setIntercomId(Long intercomId) {
        this.intercomId = intercomId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.user_id
     *
     * @return the value of intercom_detail.user_id
     *
     * @mbg.generated
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.user_id
     *
     * @param userId the value for intercom_detail.user_id
     *
     * @mbg.generated
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.team_num
     *
     * @return the value of intercom_detail.team_num
     *
     * @mbg.generated
     */
    public String getTeamNum() {
        return teamNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.team_num
     *
     * @param teamNum the value for intercom_detail.team_num
     *
     * @mbg.generated
     */
    public void setTeamNum(String teamNum) {
        this.teamNum = teamNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.start_time
     *
     * @return the value of intercom_detail.start_time
     *
     * @mbg.generated
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.start_time
     *
     * @param startTime the value for intercom_detail.start_time
     *
     * @mbg.generated
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.end_time
     *
     * @return the value of intercom_detail.end_time
     *
     * @mbg.generated
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.end_time
     *
     * @param endTime the value for intercom_detail.end_time
     *
     * @mbg.generated
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.free
     *
     * @return the value of intercom_detail.free
     *
     * @mbg.generated
     */
    public Short getFree() {
        return free;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.free
     *
     * @param free the value for intercom_detail.free
     *
     * @mbg.generated
     */
    public void setFree(Short free) {
        this.free = free;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.count_flag
     *
     * @return the value of intercom_detail.count_flag
     *
     * @mbg.generated
     */
    public Short getCountFlag() {
        return countFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.count_flag
     *
     * @param countFlag the value for intercom_detail.count_flag
     *
     * @mbg.generated
     */
    public void setCountFlag(Short countFlag) {
        this.countFlag = countFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.user_count
     *
     * @return the value of intercom_detail.user_count
     *
     * @mbg.generated
     */
    public Integer getUserCount() {
        return userCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.user_count
     *
     * @param userCount the value for intercom_detail.user_count
     *
     * @mbg.generated
     */
    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column intercom_detail.use_time
     *
     * @return the value of intercom_detail.use_time
     *
     * @mbg.generated
     */
    public Float getUseTime() {
        return useTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column intercom_detail.use_time
     *
     * @param useTime the value for intercom_detail.use_time
     *
     * @mbg.generated
     */
    public void setUseTime(Float useTime) {
        this.useTime = useTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table intercom_detail
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static IntercomDetail.Builder builder() {
        return new IntercomDetail.Builder();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table intercom_detail
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public boolean isSelective() {
        return this.selectiveColumns.size() > 0;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table intercom_detail
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public boolean isSelective(String column) {
        return this.selectiveColumns.get(column) != null;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table intercom_detail
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public IntercomDetail selective(Column ... columns) {
        this.selectiveColumns.clear();
        if (columns != null) {
            for (Column column : columns) {
                this.selectiveColumns.put(column.value(), true);
            }
        }
        return this;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table intercom_detail
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static class Builder {
        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private IntercomDetail obj;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder() {
            this.obj = new IntercomDetail();
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.intercom_id
         *
         * @param intercomId the value for intercom_detail.intercom_id
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder intercomId(Long intercomId) {
            obj.setIntercomId(intercomId);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.user_id
         *
         * @param userId the value for intercom_detail.user_id
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder userId(Long userId) {
            obj.setUserId(userId);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.team_num
         *
         * @param teamNum the value for intercom_detail.team_num
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder teamNum(String teamNum) {
            obj.setTeamNum(teamNum);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.start_time
         *
         * @param startTime the value for intercom_detail.start_time
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder startTime(Date startTime) {
            obj.setStartTime(startTime);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.end_time
         *
         * @param endTime the value for intercom_detail.end_time
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder endTime(Date endTime) {
            obj.setEndTime(endTime);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.free
         *
         * @param free the value for intercom_detail.free
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder free(Short free) {
            obj.setFree(free);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.count_flag
         *
         * @param countFlag the value for intercom_detail.count_flag
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder countFlag(Short countFlag) {
            obj.setCountFlag(countFlag);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.user_count
         *
         * @param userCount the value for intercom_detail.user_count
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder userCount(Integer userCount) {
            obj.setUserCount(userCount);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column intercom_detail.use_time
         *
         * @param useTime the value for intercom_detail.use_time
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder useTime(Float useTime) {
            obj.setUseTime(useTime);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public IntercomDetail build() {
            return this.obj;
        }
    }

    /**
     * This enum was generated by MyBatis Generator.
     * This enum corresponds to the database table intercom_detail
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public enum Column {
        intercomId("intercom_id"),
        userId("user_id"),
        teamNum("team_num"),
        startTime("start_time"),
        endTime("end_time"),
        free("free"),
        countFlag("count_flag"),
        userCount("user_count"),
        useTime("use_time");

        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private final String column;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String value() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String getValue() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        Column(String column) {
            this.column = column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String desc() {
            return this.column + " DESC";
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table intercom_detail
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String asc() {
            return this.column + " ASC";
        }
    }
}