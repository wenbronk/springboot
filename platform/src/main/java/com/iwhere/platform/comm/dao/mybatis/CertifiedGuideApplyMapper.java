package com.iwhere.platform.comm.dao.mybatis;

import com.iwhere.platform.comm.dao.mybatis.example.CertifiedGuideApplyExample;
import com.iwhere.platform.comm.dao.mybatis.model.CertifiedGuideApply;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CertifiedGuideApplyMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    long countByExample(CertifiedGuideApplyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    int deleteByExample(CertifiedGuideApplyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Long applyId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    int insert(CertifiedGuideApply record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    int insertSelective(CertifiedGuideApply record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    List<CertifiedGuideApply> selectByExample(CertifiedGuideApplyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    List<CertifiedGuideApply> selectByExampleSelective(@Param("example") CertifiedGuideApplyExample example, @Param("selective") CertifiedGuideApply.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    CertifiedGuideApply selectByPrimaryKey(Long applyId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    CertifiedGuideApply selectByPrimaryKeySelective(@Param("applyId") Long applyId, @Param("selective") CertifiedGuideApply.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") CertifiedGuideApply record, @Param("example") CertifiedGuideApplyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") CertifiedGuideApply record, @Param("example") CertifiedGuideApplyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(CertifiedGuideApply record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(CertifiedGuideApply record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    CertifiedGuideApply selectOneByExample(CertifiedGuideApplyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    CertifiedGuideApply selectOneByExampleSelective(@Param("example") CertifiedGuideApplyExample example, @Param("selective") CertifiedGuideApply.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int batchInsert(@Param("list") List<CertifiedGuideApply> list);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int batchInsertSelective(@Param("list") List<CertifiedGuideApply> list, @Param("selective") CertifiedGuideApply.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int upsert(CertifiedGuideApply record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table certified_guide_apply
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int upsertSelective(CertifiedGuideApply record);
}