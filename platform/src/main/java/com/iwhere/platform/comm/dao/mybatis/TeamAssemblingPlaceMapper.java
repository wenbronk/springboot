package com.iwhere.platform.comm.dao.mybatis;

import com.iwhere.platform.comm.dao.mybatis.example.TeamAssemblingPlaceExample;
import com.iwhere.platform.comm.dao.mybatis.model.TeamAssemblingPlace;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TeamAssemblingPlaceMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    long countByExample(TeamAssemblingPlaceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    int deleteByExample(TeamAssemblingPlaceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Long assemblyId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    int insert(TeamAssemblingPlace record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    int insertSelective(TeamAssemblingPlace record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    List<TeamAssemblingPlace> selectByExample(TeamAssemblingPlaceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    List<TeamAssemblingPlace> selectByExampleSelective(@Param("example") TeamAssemblingPlaceExample example, @Param("selective") TeamAssemblingPlace.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    TeamAssemblingPlace selectByPrimaryKey(Long assemblyId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    TeamAssemblingPlace selectByPrimaryKeySelective(@Param("assemblyId") Long assemblyId, @Param("selective") TeamAssemblingPlace.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") TeamAssemblingPlace record, @Param("example") TeamAssemblingPlaceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") TeamAssemblingPlace record, @Param("example") TeamAssemblingPlaceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TeamAssemblingPlace record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TeamAssemblingPlace record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    TeamAssemblingPlace selectOneByExample(TeamAssemblingPlaceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    TeamAssemblingPlace selectOneByExampleSelective(@Param("example") TeamAssemblingPlaceExample example, @Param("selective") TeamAssemblingPlace.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int batchInsert(@Param("list") List<TeamAssemblingPlace> list);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int batchInsertSelective(@Param("list") List<TeamAssemblingPlace> list, @Param("selective") TeamAssemblingPlace.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int upsert(TeamAssemblingPlace record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int upsertSelective(TeamAssemblingPlace record);
}