package com.iwhere.platform.comm.dao.mybatis.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table team_assembling_place
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class TeamAssemblingPlace {
    /**
     * Database Column Remarks:
     *   集合地点ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_assembling_place.assembly_id
     *
     * @mbg.generated
     */
    private Long assemblyId;

    /**
     * Database Column Remarks:
     *   团队编号
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_assembling_place.team_num
     *
     * @mbg.generated
     */
    private String teamNum;

    /**
     * Database Column Remarks:
     *   用户ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_assembling_place.user_id
     *
     * @mbg.generated
     */
    private Long userId;

    /**
     * Database Column Remarks:
     *   经度
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_assembling_place.lng
     *
     * @mbg.generated
     */
    private Double lng;

    /**
     * Database Column Remarks:
     *   纬度
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_assembling_place.lat
     *
     * @mbg.generated
     */
    private Double lat;

    /**
     * Database Column Remarks:
     *   集合时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_assembling_place.assemble_time
     *
     * @mbg.generated
     */
    private Date assembleTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    private Map<String, Boolean> selectiveColumns = new HashMap<String, Boolean>();

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_assembling_place.assembly_id
     *
     * @return the value of team_assembling_place.assembly_id
     *
     * @mbg.generated
     */
    public Long getAssemblyId() {
        return assemblyId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_assembling_place.assembly_id
     *
     * @param assemblyId the value for team_assembling_place.assembly_id
     *
     * @mbg.generated
     */
    public void setAssemblyId(Long assemblyId) {
        this.assemblyId = assemblyId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_assembling_place.team_num
     *
     * @return the value of team_assembling_place.team_num
     *
     * @mbg.generated
     */
    public String getTeamNum() {
        return teamNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_assembling_place.team_num
     *
     * @param teamNum the value for team_assembling_place.team_num
     *
     * @mbg.generated
     */
    public void setTeamNum(String teamNum) {
        this.teamNum = teamNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_assembling_place.user_id
     *
     * @return the value of team_assembling_place.user_id
     *
     * @mbg.generated
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_assembling_place.user_id
     *
     * @param userId the value for team_assembling_place.user_id
     *
     * @mbg.generated
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_assembling_place.lng
     *
     * @return the value of team_assembling_place.lng
     *
     * @mbg.generated
     */
    public Double getLng() {
        return lng;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_assembling_place.lng
     *
     * @param lng the value for team_assembling_place.lng
     *
     * @mbg.generated
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_assembling_place.lat
     *
     * @return the value of team_assembling_place.lat
     *
     * @mbg.generated
     */
    public Double getLat() {
        return lat;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_assembling_place.lat
     *
     * @param lat the value for team_assembling_place.lat
     *
     * @mbg.generated
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_assembling_place.assemble_time
     *
     * @return the value of team_assembling_place.assemble_time
     *
     * @mbg.generated
     */
    public Date getAssembleTime() {
        return assembleTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_assembling_place.assemble_time
     *
     * @param assembleTime the value for team_assembling_place.assemble_time
     *
     * @mbg.generated
     */
    public void setAssembleTime(Date assembleTime) {
        this.assembleTime = assembleTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static TeamAssemblingPlace.Builder builder() {
        return new TeamAssemblingPlace.Builder();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public boolean isSelective() {
        return this.selectiveColumns.size() > 0;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public boolean isSelective(String column) {
        return this.selectiveColumns.get(column) != null;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public TeamAssemblingPlace selective(Column ... columns) {
        this.selectiveColumns.clear();
        if (columns != null) {
            for (Column column : columns) {
                this.selectiveColumns.put(column.value(), true);
            }
        }
        return this;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static class Builder {
        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private TeamAssemblingPlace obj;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder() {
            this.obj = new TeamAssemblingPlace();
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_assembling_place.assembly_id
         *
         * @param assemblyId the value for team_assembling_place.assembly_id
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder assemblyId(Long assemblyId) {
            obj.setAssemblyId(assemblyId);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_assembling_place.team_num
         *
         * @param teamNum the value for team_assembling_place.team_num
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder teamNum(String teamNum) {
            obj.setTeamNum(teamNum);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_assembling_place.user_id
         *
         * @param userId the value for team_assembling_place.user_id
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder userId(Long userId) {
            obj.setUserId(userId);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_assembling_place.lng
         *
         * @param lng the value for team_assembling_place.lng
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder lng(Double lng) {
            obj.setLng(lng);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_assembling_place.lat
         *
         * @param lat the value for team_assembling_place.lat
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder lat(Double lat) {
            obj.setLat(lat);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column team_assembling_place.assemble_time
         *
         * @param assembleTime the value for team_assembling_place.assemble_time
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder assembleTime(Date assembleTime) {
            obj.setAssembleTime(assembleTime);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public TeamAssemblingPlace build() {
            return this.obj;
        }
    }

    /**
     * This enum was generated by MyBatis Generator.
     * This enum corresponds to the database table team_assembling_place
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public enum Column {
        assemblyId("assembly_id"),
        teamNum("team_num"),
        userId("user_id"),
        lng("lng"),
        lat("lat"),
        assembleTime("assemble_time");

        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private final String column;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String value() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String getValue() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        Column(String column) {
            this.column = column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String desc() {
            return this.column + " DESC";
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table team_assembling_place
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String asc() {
            return this.column + " ASC";
        }
    }
}