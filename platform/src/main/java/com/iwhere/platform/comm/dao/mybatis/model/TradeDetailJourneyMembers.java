package com.iwhere.platform.comm.dao.mybatis.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table trade_detail_journey_members
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class TradeDetailJourneyMembers {
    /**
     * Database Column Remarks:
     *   商户订单号
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column trade_detail_journey_members.trade_no
     *
     * @mbg.generated
     */
    private String tradeNo;

    /**
     * Database Column Remarks:
     *   用户姓名
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column trade_detail_journey_members.user_name
     *
     * @mbg.generated
     */
    private String userName;

    /**
     * Database Column Remarks:
     *   联系方式
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column trade_detail_journey_members.contact
     *
     * @mbg.generated
     */
    private String contact;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table trade_detail_journey_members
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    private Map<String, Boolean> selectiveColumns = new HashMap<String, Boolean>();

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column trade_detail_journey_members.trade_no
     *
     * @return the value of trade_detail_journey_members.trade_no
     *
     * @mbg.generated
     */
    public String getTradeNo() {
        return tradeNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column trade_detail_journey_members.trade_no
     *
     * @param tradeNo the value for trade_detail_journey_members.trade_no
     *
     * @mbg.generated
     */
    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column trade_detail_journey_members.user_name
     *
     * @return the value of trade_detail_journey_members.user_name
     *
     * @mbg.generated
     */
    public String getUserName() {
        return userName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column trade_detail_journey_members.user_name
     *
     * @param userName the value for trade_detail_journey_members.user_name
     *
     * @mbg.generated
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column trade_detail_journey_members.contact
     *
     * @return the value of trade_detail_journey_members.contact
     *
     * @mbg.generated
     */
    public String getContact() {
        return contact;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column trade_detail_journey_members.contact
     *
     * @param contact the value for trade_detail_journey_members.contact
     *
     * @mbg.generated
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trade_detail_journey_members
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static TradeDetailJourneyMembers.Builder builder() {
        return new TradeDetailJourneyMembers.Builder();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trade_detail_journey_members
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public boolean isSelective() {
        return this.selectiveColumns.size() > 0;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trade_detail_journey_members
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public boolean isSelective(String column) {
        return this.selectiveColumns.get(column) != null;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trade_detail_journey_members
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public TradeDetailJourneyMembers selective(Column ... columns) {
        this.selectiveColumns.clear();
        if (columns != null) {
            for (Column column : columns) {
                this.selectiveColumns.put(column.value(), true);
            }
        }
        return this;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table trade_detail_journey_members
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public static class Builder {
        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private TradeDetailJourneyMembers obj;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder() {
            this.obj = new TradeDetailJourneyMembers();
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column trade_detail_journey_members.trade_no
         *
         * @param tradeNo the value for trade_detail_journey_members.trade_no
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder tradeNo(String tradeNo) {
            obj.setTradeNo(tradeNo);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column trade_detail_journey_members.user_name
         *
         * @param userName the value for trade_detail_journey_members.user_name
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder userName(String userName) {
            obj.setUserName(userName);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column trade_detail_journey_members.contact
         *
         * @param contact the value for trade_detail_journey_members.contact
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public Builder contact(String contact) {
            obj.setContact(contact);
            return this;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public TradeDetailJourneyMembers build() {
            return this.obj;
        }
    }

    /**
     * This enum was generated by MyBatis Generator.
     * This enum corresponds to the database table trade_detail_journey_members
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    public enum Column {
        tradeNo("trade_no"),
        userName("user_name"),
        contact("contact");

        /**
         * This field was generated by MyBatis Generator.
         * This field corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        private final String column;

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String value() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String getValue() {
            return this.column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        Column(String column) {
            this.column = column;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String desc() {
            return this.column + " DESC";
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method corresponds to the database table trade_detail_journey_members
         *
         * @mbg.generated
         * @project https://github.com/itfsw/mybatis-generator-plugin
         */
        public String asc() {
            return this.column + " ASC";
        }
    }
}