package com.iwhere.platform.comm.dao.mongodb.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/30 20:29
 * ---------------------------------------------------------------------------
 */
@Data
@Builder
@Document(collection = "iw-client")
public class ClientEntity {
    @Id
    private String clientId;    // 客户端ID
    private Long count;
}
