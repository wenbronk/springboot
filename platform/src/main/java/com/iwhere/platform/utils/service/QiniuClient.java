package com.iwhere.platform.utils.service;

import com.qiniu.util.Auth;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/14 16:46
 * ---------------------------------------------------------------------------
 */
public class QiniuClient {
    private Auth auth;
    private String bucket;

    /**
     * 构造函数
     * @param accessKey
     * @param secretKey
     * @param bucket
     */
    public QiniuClient(String accessKey, String secretKey, String bucket) {
        this.auth = Auth.create(accessKey, secretKey);
        this.bucket = bucket;
    }

    /**
     * Getter method for property <tt>auth</tt>.
     * @return property value of auth
     * @author hewei
     */
    public Auth getAuth() {
        return auth;
    }

    /**
     * Getter method for property <tt>bucket</tt>.
     * @return property value of bucket
     * @author hewei
     */
    public String getBucket() {
        return bucket;
    }
}
