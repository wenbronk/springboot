package com.iwhere.platform.utils.exception;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/2 14:50
 * ---------------------------------------------------------------------------
 */
public enum Exceptions {
    // ------------------------------ 基础 ------------------------------
    SMS_SEND_TOO_FAST(500000001, "该设备短信发送过于频繁！"),

    //---------------------团队-------------------------------------
    TEAM_INVITE_USER_NOT_EXIST(500100001,"发起者用户不存在"),
    TEAM_NOT_EXIST(500100002,"团队不存在"),
    TEAM_USER_NOT_EXIST(500100003,"不存在该团员"),
    TEAM_USER_NOT_CREATE(500100004,"该团员不是团队创建者"),
    TEAM_USER_EXIST(500100005,"该团员已存在"),
    TEAM_USER_NO_PERMISSION(500100006,"该用户没有此权限"),
    TEAM_FENCE_SET_ERROR(500100007,"围栏设置参数异常"),
    TEAM_CREATE(500100008, "创建团队失败,团队已存在"),
    TEAM_FENCE_NOT_IN_AREA(500100009,"没有在景区范围"),
    TEAM_USER_HAVE_REMOVED(500100010,"你已被移出团"),

    //---------------------------对讲-----------------------------------
    INTERCOM_NO_PERMISSION_FREE_APPLY(500101001,"没有免费申请对讲权限"),
	INTERCOM_NO_TIME(500101002,"已无剩余对讲时长"),
    INTERCOM_NOT_EXIST(500101003,"不存在此对讲信息"),
    INTERCOM_TIME_OVER(500101004,"对讲时长超过剩余对讲时长"),

	//-------------------------用户--------------------------------------
	USER_NOT_EXIST(500102001,"用户不存在"),

    // ------------------------------ 支付 ------------------------------
    PAY_ERROR_TRADE_NO(500103001, "错误的订单号！"),
    PAY_CREATE_WX_TRADE_ERROR(500103002, "创建微信订单失败！"),
    PAY_ERROR_SIGN(500103003, "错误的签名！"),
 // ------------------------------ 其他------------------------------
    OTHER_NO_UPLOAD(500104001, "上传北京足迹失败"),
    ;


    private final int code; // error code
    private final String msg;   // error msg

    /**
     * 构造函数
     * @param code
     * @param msg
     */
    Exceptions(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 异常
     * @return
     */
    public IWException exception() {
        return new IWException(this.code, this.msg);
    }

    /**
     * Getter method for property <tt>code</tt>.
     * @return property value of code
     * @author hewei
     */
    public int getCode() {
        return code;
    }

    /**
     * Getter method for property <tt>msg</tt>.
     * @return property value of msg
     * @author hewei
     */
    public String getMsg() {
        return msg;
    }
}
