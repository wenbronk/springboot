package com.iwhere.platform.utils.exception;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * <strong>copyright</strong>： ©版权所有 成都都在哪网讯科技有限公司<br>
 * ----------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/7/23 14:31
 * ---------------------------------------------------------------------------
 */
public class IWException extends RuntimeException{
    private int code = 500;   // 异常Code
    private String errorMsg = "通用异常！";  // 异常信息

    /**
     * 构造函数
     * @param code 异常编码
     */
    public IWException(int code) {
        super();
        this.code = code;
    }

    /**
     * 构造函数
     * @param errorMsg 异常信息
     */
    public IWException(String errorMsg){
        super(errorMsg);
        this.errorMsg = errorMsg;
    }

    /**
     * 构造函数
     * @param code 异常编码
     * @param errorMsg 异常信息
     */
    public IWException(int code, String errorMsg){
        super(errorMsg);
        this.code = code;
        this.errorMsg = errorMsg;
    }

    /**
     * Getter method for property <tt>code</tt>.
     * @return property value of code
     * @author hewei
     */
    public int getCode() {
        return code;
    }

    /**
     * Setter method for property <tt>code</tt>.
     * @param code value to be assigned to property code
     * @author hewei
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Getter method for property <tt>errorMsg</tt>.
     * @return property value of errorMsg
     * @author hewei
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * Setter method for property <tt>errorMsg</tt>.
     * @param errorMsg value to be assigned to property errorMsg
     * @author hewei
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
