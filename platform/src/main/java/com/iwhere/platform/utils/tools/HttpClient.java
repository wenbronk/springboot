package com.iwhere.platform.utils.tools;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * ---------------------------------------------------------------------------
 * Http Client
 * ---------------------------------------------------------------------------
 * <strong>copyright</strong>： ©版权所有 成都都在哪网讯科技有限公司<br>
 * ----------------------------------------------------------------------------
 * @author: hewei
 * @time:2016/12/23 11:34
 * ---------------------------------------------------------------------------
 */
public class HttpClient {
    private final static OkHttpClient client; // httpClient
    private final static ObjectMapper mapper;   // mapper

    static {
        client = new OkHttpClient()
                .newBuilder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .build();
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * 发送POST请求(返回数据JSON转换)
     * @param url       url 地址
     * @param params    参数
     * @param valueType 需要转换的类型
     * @return
     * @throws IOException
     */
    public static <T> T doPost(String url, Map<String, Object> params, Class<T> valueType) throws IOException {
        // 1. 请求
        Response resp = doPost(url, params);
        if (resp.isSuccessful()) {
            // 2. JSON 转换
            String body = resp.body().string();
            resp.close();
            return mapper.readValue(body, valueType);
        } else {
            resp.close();
            return null;
        }
    }

    /**
     * 发送GET请求(返回数据JSON转换)
     * @param url       url 地址
     * @param params    参数
     * @param valueType 需要转换的类型
     * @return
     * @throws IOException
     */
    public static <T> T doGet(String url, Map<String, Object> params, Class<T> valueType) throws IOException {
        // 1. 请求
        Response resp = doGet(url, params);
        if (resp.isSuccessful()) {
            // 2. JSON 转换
            String body = resp.body().string();
            resp.close();
            return mapper.readValue(body, valueType);
        } else {
            resp.close();
            return null;
        }
    }

    /**
     * 发送POST请求
     * @param url    url 地址
     * @param params 参数
     * @return
     * @throws IOException
     * @author hewei
     */
    public static Response doPost(String url, Map<String, Object> params) throws IOException {
        // 2. 构建一个请求
        // 参数封装
        FormBody.Builder formBuilder = new FormBody.Builder();
        for (String key : params.keySet()) {
            formBuilder.add(key, params.get(key).toString());
        }
        Request request = new Request.Builder().url(url).post(formBuilder.build()).build();
        // 3. 创建一个Call
        final Call call = client.newCall(request);
        // 4. 执行请求
        Response resp = call.execute();
        return resp;
    }

    /**
     * 发送GET请求
     * @param url    url 地址
     * @param params 参数
     * @return
     * @throws IOException
     * @author hewei
     */
    public static Response doGet(String url, Map<String, Object> params) throws IOException {
        // 2. 构建一个请求
        // 参数封装
        StringBuilder tempParams = new StringBuilder();
        int pos = 0;
        for (String key : params.keySet()) {
            if (pos > 0) {
                tempParams.append("&");
            }
            tempParams.append(String.format("%s=%s", key, URLEncoder.encode(params.get(key).toString(), "utf-8")));
            pos++;
        }
        String reqUrl = String.format("%s%s%s", url, url.matches(".*\\?.*") ? "" : "?", tempParams);

        Request request = new Request.Builder().url(reqUrl).get().build();
        // 3. 创建一个Call
        final Call call = client.newCall(request);
        // 4. 执行请求
        Response resp = call.execute();
        return resp;
    }
}
