package com.iwhere.platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * <strong>copyright</strong>： ©版权所有 北京都在哪网讯科技有限公司成都分公司<br>
 * ----------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/4/6 16:22
 * ---------------------------------------------------------------------------
 */
@SpringBootApplication
@EnableAutoConfiguration  //开启自动配置
public class Application {
    /**
     * Main Start
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
