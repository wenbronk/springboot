package com.iwhere.platform.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

/**
 * ---------------------------------------------------------------------------
 * MongoDB 配置
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/4/11 13:27
 * ---------------------------------------------------------------------------
 */
@Configuration
@EnableMongoAuditing
public class MongoDBConfig {
}
