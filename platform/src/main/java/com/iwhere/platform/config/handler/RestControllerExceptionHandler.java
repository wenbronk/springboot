package com.iwhere.platform.config.handler;


import com.iwhere.platform.utils.base.dto.BaseResp;
import com.iwhere.platform.utils.exception.IWException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.List;

/**
 * ---------------------------------------------------------------------------
 * 统一异常Handler
 * ---------------------------------------------------------------------------
 * <strong>copyright</strong>： ©版权所有 成都都在哪网讯科技有限公司<br>
 * ----------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/7/23 10:25
 * ---------------------------------------------------------------------------
 */
@RestControllerAdvice
@Slf4j
public class RestControllerExceptionHandler {

    /**
     * 通用Exception异常
     * @return
     * @author hewei
     */
    @ExceptionHandler(value = Exception.class)
    public BaseResp exceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception ex) {
        autoRecordRequest(request, ex);
        return BaseResp.error();
    }

    /**
     * 自定义异常
     * @return
     * @author hewei
     */
    @ExceptionHandler(value = IWException.class)
    public BaseResp exceptionHandler(HttpServletRequest request, HttpServletResponse response, IWException ex) {
        autoRecordRequest(request, ex);
        return BaseResp.error(ex.getCode(), ex.getErrorMsg());
    }

    /**
     * 验证异常
     * @return
     * @author hewei
     */
    @ExceptionHandler(value = BindException.class)
    public BaseResp exceptionHandler(HttpServletRequest request, HttpServletResponse response, BindException ex) {
        autoRecordRequest(request, ex);

        StringBuffer sb = new StringBuffer();
        List<FieldError> errors = ex.getFieldErrors();
        int count = errors.size();
        for (int i = 0; i < errors.size(); i++) {
            FieldError error = errors.get(i);
            sb.append(error.getField());    // 字段名称
            sb.append(error.getDefaultMessage());   // 异常信息
            if (i < count - 1) {
                sb.append("#");
            }
        }
        return BaseResp.error(510, sb.toString());
    }

    /**
     * 自动记录请求信息
     * @param request
     * @param ex
     * @author hewei
     */
    private void autoRecordRequest(HttpServletRequest request, Exception ex) {
        StringBuffer sb = new StringBuffer();
        sb.append(request.getRequestURL().toString());
        Enumeration<String> names = request.getParameterNames();
        boolean flag = true;
        while (names.hasMoreElements()) {
            sb.append(flag ? "?" : "&");
            flag = false;
            String name = names.nextElement();
            sb.append(name);
            sb.append("=");
            sb.append(request.getParameter(name));
        }
        // 记录异常
        log.error(sb.toString());
        log.error(ex.getLocalizedMessage(), ex);
    }
}
