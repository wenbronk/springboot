package com.iwhere.platform.config;


import com.iwhere.platform.config.converter.JsonStringToListMapArrayObjectConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/14 19:45
 * ---------------------------------------------------------------------------
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter{

    /**
     * @see WebMvcConfigurerAdapter
     * @param registry
     * @author hewei
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        super.addFormatters(registry);
        // json参数解析
        registry.addConverter(new JsonStringToListMapArrayObjectConverter());
    }

    /**
     * @see WebMvcConfigurerAdapter
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //将所有/static/** 访问都映射到classpath:/static/ 目录下
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }
}
