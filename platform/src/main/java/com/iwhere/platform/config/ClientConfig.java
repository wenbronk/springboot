package com.iwhere.platform.config;


import com.iwhere.platform.config.properties.QiniuProperties;
import com.iwhere.platform.utils.service.QiniuClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ---------------------------------------------------------------------------
 * 第三方客户端配置
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/25 16:25
 * ---------------------------------------------------------------------------
 */
@Configuration
@EnableConfigurationProperties({QiniuProperties.class})
public class ClientConfig {
    /**
     * 七牛
     * @return
     * @author hewei
     */
    @Bean
    public QiniuClient qiniuClient(@Autowired QiniuProperties properties) {
        return new QiniuClient(properties.getAccessKey(), properties.getSecretKey(), properties.getBucket());
    }
}
