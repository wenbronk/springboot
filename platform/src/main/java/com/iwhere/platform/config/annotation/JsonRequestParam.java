package com.iwhere.platform.config.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ---------------------------------------------------------------------------
 * Json参数注解
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/29 18:27
 * ---------------------------------------------------------------------------
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonRequestParam {
}
