package com.iwhere.platform.config;


import com.iwhere.platform.config.properties.MybatisProperties;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * <strong>copyright</strong>： ©版权所有 北京都在哪网讯科技有限公司成都分公司<br>
 * ----------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/7 17:54
 * ---------------------------------------------------------------------------
 */
@Configuration
@AutoConfigureAfter(DruidConfig.class)
@EnableConfigurationProperties(MybatisProperties.class)
@MapperScan(basePackages = {"com.iwhere.platform.biz.**.dao.mybatis", "com.iwhere.platform.comm.dao.mybatis"})
@EnableTransactionManagement
public class MybatisConfig {

    /**
     * SqlSessionFactory
     * @return
     * @author hewei
     */
    @Bean
    public SqlSessionFactory sqlSessionFactory(@Autowired DataSource dataSource, @Autowired MybatisProperties properties) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        // 1.设置数据源
        sqlSessionFactoryBean.setDataSource(dataSource);
        // 2.设置mapper信息
        sqlSessionFactoryBean.setMapperLocations(properties.resolveMapperLocations());

        // !!!! callSettersOnNulls
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setCallSettersOnNulls(true);
        // 驼峰命名
        configuration.setMapUnderscoreToCamelCase(true);
        sqlSessionFactoryBean.setConfiguration(configuration);

        return sqlSessionFactoryBean.getObject();
    }

    /**
     * 事务管理器
     * @param dataSource
     * @return
     * @author hewei
     */
    @Bean
    public PlatformTransactionManager txManager(@Autowired DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}