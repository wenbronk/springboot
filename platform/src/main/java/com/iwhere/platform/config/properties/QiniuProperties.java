package com.iwhere.platform.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * ---------------------------------------------------------------------------
 *
 * ---------------------------------------------------------------------------
 * @author: hewei
 * @time:2017/8/1 13:49
 * ---------------------------------------------------------------------------
 */
@ConfigurationProperties(prefix = QiniuProperties.PREFIX)
@Data
public class QiniuProperties {
    public static final String PREFIX = "qiniu";

    private String bucket;
    private String accessKey;
    private String secretKey;
}
