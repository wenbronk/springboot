package platform;

import platform.Employee;

public class Main {

	public static void main(String[] args) {
		Employee emp = Employee.builder().empName("Deendaya").salary(100).build();
		System.out.println(emp);
	}

}
